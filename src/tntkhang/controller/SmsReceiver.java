package tntkhang.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tntkhang.database.BlackListHelper;
import tntkhang.database.ContactHelper;
import tntkhang.database.LockWordHelper;
import tntkhang.database.MissSmsHelper;
import tntkhang.database.SettingHelper;
import tntkhang.database.WhiteListHelper;
import tntkhang.model.ContactModel;
import tntkhang.model.LockwordModel;
import tntkhang.model.MissCallModel;
import tntkhang.model.MissSmsModel;
import tntkhang.model.SettingConfigModel;

import com.nonglamuniversity.disturbcall.R;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

/**
 * @author KHANGTRAN
 * @date Jun 6, 2013
 */

public class SmsReceiver extends BroadcastReceiver {

	public static final String SMS_EXTRA_NAME = "pdus";
	LockWordHelper lwHelper;
	ArrayList<LockwordModel> listLockword;
	SettingConfigModel stConfig;

	BlackListHelper blackListHelper;
	WhiteListHelper whiteListHelper;
	SettingHelper settingHelper;
	ContactHelper ctHelper;
	MissSmsHelper msHelper;
	String content;
	String phonenumber;
	String date;
	String time;

	// String[] specSymbol = { "!", "@", "#", "$", "/", "^", "&", "*", "(", ")",
	// "-", ";", ":", "'", "\"", ",", "?", "+", "_", "%", "=", "<", ">",
	// "{", "}", "[", "]", "." };

	@Override
	public void onReceive(Context context, Intent intent) {
		lwHelper = new LockWordHelper(context);
		listLockword = lwHelper.loadLockWord();
		blackListHelper = new BlackListHelper(context);
		whiteListHelper = new WhiteListHelper(context);
		settingHelper = new SettingHelper(context);
		ctHelper = new ContactHelper(context);
		msHelper = new MissSmsHelper(context);
		stConfig = settingHelper.loadSetting();

		// if(isOrderedBroadcast()){
		// Toast t = Toast.makeText(context, "Order", Toast.LENGTH_LONG);
		// t.setDuration(300);
		// t.show();
		// Log.i("a","Order");
		//
		// }else{
		// Toast.makeText(context, "NOOOOOOOOOOO Order",
		// Toast.LENGTH_LONG).show();
		// Log.i("a","NOOOOOOOOOOOOO  Order");
		// }
		Intent smsRecvIntent = new Intent(
				"android.provider.Telephony.SMS_RECEIVED");
		List<ResolveInfo> infos = context.getPackageManager()
				.queryBroadcastReceivers(smsRecvIntent, 0);
		for (ResolveInfo info : infos) {
			Log.i("TEST", "Receiver: " + info.activityInfo.name + ", priority="
					+ info.priority);
			if (info.priority == 166) {
			}
		}
		Bundle extras = intent.getExtras();
		if (extras != null) {
			Object[] smsExtra = (Object[]) extras.get(SMS_EXTRA_NAME);

			// String starWith = settingHelper.loadSetting().startWith+"";
			// String endWith = settingHelper.loadSetting().endWith+"";
			for (int i = 0; i < smsExtra.length; i++) {
				SmsMessage message = SmsMessage
						.createFromPdu((byte[]) smsExtra[i]);
				content = message.getMessageBody().toString();
				phonenumber = message.getOriginatingAddress();
				date = Utility.getInstance().getDateFromTimestamp(
						message.getTimestampMillis());
				time = Utility.getInstance().getTimeFromTimestamp(
						message.getTimestampMillis());

				// ctHelper.addContact(new Contact(IDPM, phonenumber,
				// "*Chặn từ khóa", 2));
				// saveMissSms("aa");
				// this.abortBroadcast();

				if (isExitKeyword(content, listLockword) != null) {
					// ctHelper.addContact(new Contact(IDPM, phonenumber,
					// "*Chặn từ khóa", 2));
					saveMissSms(context.getString(R.string.lock_work));
					this.abortBroadcast();
				}

				// chan blacklist va whitelist
				if (settingHelper.loadSetting().enable == 1) {
					if (blackListHelper.isExitInBlackList(phonenumber) == true) {
						if (ctHelper.getContactByPhonenumber(phonenumber).blocktype > 1) {
							saveMissSms(ctHelper
									.getContactByPhonenumber(phonenumber).name);
							this.abortBroadcast();
						}
					}
				} else if (settingHelper.loadSetting().enable == 2) {
					if (whiteListHelper.isExitInWhiteList(phonenumber) == false) {
						// if(ctHelper.getContactByPhonenumber(phonenumber)==null){
						// ctHelper.addContact(new Contact(IDPM, phonenumber,
						// "*Ngoài ds cho phép", 2));
						// }
						saveMissSms(context.getString(R.string.out_of_list));
						this.abortBroadcast();
					}
				}

				String ba = phonenumber.substring(0, 3);
				String bon = phonenumber.substring(0, 4);

				// CHan mang VIETTEL
				if (stConfig.viettel == 1) {
					if (ba.equalsIgnoreCase("096")
							|| ba.equalsIgnoreCase("097")
							|| ba.equalsIgnoreCase("098")
							|| ba.equalsIgnoreCase("016")) {
						// Ket thuc TIN NHAN
						// if(ctHelper.getContactByPhonenumber(phonenumber)==null){
						// ctHelper.addContact(new Contact(IDPM, phonenumber,
						// "*Chặn Viettel", 2));
						// }
						saveMissSms(context.getString(R.string.lock_viettel));
						this.abortBroadcast();
					}
				} else if (stConfig.vina == 1) {
					if (ba.equalsIgnoreCase("091")
							|| ba.equalsIgnoreCase("094")
							|| bon.equalsIgnoreCase("0123")
							|| bon.equalsIgnoreCase("0124")
							|| bon.equalsIgnoreCase("0125")
							|| bon.equalsIgnoreCase("0127")
							|| bon.equalsIgnoreCase("0129")) {
						// CHan Mnag VINA
						// if(ctHelper.getContactByPhonenumber(phonenumber)==null){
						// ctHelper.addContact(new Contact(IDPM, phonenumber,
						// "*Chặn Vinaphone", 2));
						// }
						saveMissSms(context.getString(R.string.lock_vina));
						this.abortBroadcast();
					}
				} else if (stConfig.mobi == 1) {
					if (ba.equalsIgnoreCase("090")
							|| ba.equalsIgnoreCase("093")
							|| bon.equalsIgnoreCase("0120")
							|| bon.equalsIgnoreCase("0121")
							|| bon.equalsIgnoreCase("0122")
							|| bon.equalsIgnoreCase("0126")
							|| bon.equalsIgnoreCase("0128")) {
						// CHan Mang MOBI
						// if(ctHelper.getContactByPhonenumber(phonenumber)==null){
						// ctHelper.addContact(new Contact(IDPM, phonenumber,
						// "*Chặn Mobifone", 2));
						// }
						saveMissSms(context.getString(R.string.lock_mobi));
						this.abortBroadcast();
					}
				} else if (stConfig.vnmobile == 1) {
					if (ba.equalsIgnoreCase("092")
							|| bon.equalsIgnoreCase("0188")) {
						// CHan mang VIETNAME MOBILE
						// if(ctHelper.getContactByPhonenumber(phonenumber)==null){
						// ctHelper.addContact(new Contact(IDPM, phonenumber,
						// "*Chặn VNMobile", 2));
						// }
						saveMissSms(context.getString(R.string.lock_vnmobile));
						this.abortBroadcast();
					}
				} else if (stConfig.gmobile == 1) {
					if (ba.equalsIgnoreCase("099")
							|| bon.equalsIgnoreCase("0199")) {
						// Chan mang GMOBILE
						// if(ctHelper.getContactByPhonenumber(phonenumber)==null){
						// ctHelper.addContact(new Contact(IDPM, phonenumber,
						// "*Chặn GMobile", 2));
						// }
						saveMissSms(context.getString(R.string.lock_gmobile));
						this.abortBroadcast();
					}
				}

				// Chan tuy chon bat dau vs ket thuc

				String startW = String.valueOf("0" + stConfig.startWith);
				String endW = String.valueOf(stConfig.endWith);

				if (stConfig.startWith != 0) {
					if (phonenumber.substring(0, startW.length())
							.equals(startW)) {
						// if(ctHelper.getContactByPhonenumber(phonenumber)==null){
						// ctHelper.addContact(new Contact(IDPM, phonenumber,
						// "*Chặn đầu - " + startW, 2));
						// }
						saveMissSms(context.getString(R.string.lock_first)
								+ startW);
						this.abortBroadcast();
					}
				}
				if (stConfig.endWith != 0) {
					if (phonenumber.substring(
							phonenumber.length() - endW.length(),
							phonenumber.length()).equals(endW)) {
						// if(ctHelper.getContactByPhonenumber(phonenumber)==null){
						// ctHelper.addContact(new Contact(IDPM, phonenumber,
						// "*Chặn cuối - " + endW, 2));
						// }
						saveMissSms(context.getString(R.string.lock_last)
								+ endW);
						this.abortBroadcast();
					}
				}

			}
		}
	}

	public String isExitKeyword(String content, ArrayList<LockwordModel> listKeyword) {
		LockwordModel lockword;
		String content_keyword = "", id_keyword;
		for (int i = 0; i < listKeyword.size(); i++) {
			lockword = listKeyword.get(i);
			content_keyword = lockword.getContent();
			id_keyword = lockword.getId();
			if (content.contains(content_keyword)) {
				return id_keyword;
			}
		}
		return null;
	}

	public void saveMissSms(String name) {
		String id = Long.toHexString(Double.doubleToLongBits(Math.random()))
				.toUpperCase();

		String idCt = "";
		if (ctHelper.isExitThisNumber(phonenumber)) {
			idCt = ctHelper.getContactByPhonenumber(phonenumber).id;

		} else {
			idCt = Long.toHexString(Double.doubleToLongBits(Math.random()))
					.toUpperCase();
			ctHelper.addContact(new ContactModel(idCt, phonenumber, name, 10));
		}

		DateFormat date = new SimpleDateFormat("yyyy-MM-dd");
		Date getDate = new Date();

		DateFormat time = new SimpleDateFormat("HH:mm:ss");
		Date getTime = new Date();

		MissSmsModel mc = new MissSmsModel(id, name, phonenumber, content,
				date.format(getDate), time.format(getTime));

		msHelper.saveMissSMS(mc);
	}

}
