package tntkhang.controller;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import tntkhang.activity.PhoneCallLogsActivity;
import tntkhang.database.AutoDetectHelper;
import tntkhang.database.BlackListHelper;
import tntkhang.database.ContactHelper;
import tntkhang.database.MissCallHelper;
import tntkhang.database.SettingHelper;
import tntkhang.database.WhiteListHelper;
import tntkhang.model.CallLogModel;
import tntkhang.model.ContactModel;
import tntkhang.model.MissCallModel;
import tntkhang.model.SettingConfigModel;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;
import com.nonglamuniversity.disturbcall.R;
import com.nonglamuniversity.disturbcall.R.string;


public class CallBlock extends BroadcastReceiver {
	String incomingNumber;
	BlackListHelper blHelper;
	WhiteListHelper wlHelper;
	ContactHelper ctHelper;
	ContactModel isDisturb;
	Context context;
	private static long startSecOne;
	private static long endSecOne;
	private static long startSecTwo;
	private static long endSecTwo;
	
	private static long longSecOne;
	private static long longSecTwo;

	ITelephony telephonyService;
	private static final String TAG = "Phone call";
	SettingHelper stHelper;
	MissCallHelper mcHelper;
	SettingConfigModel stConfig;
	AutoDetectHelper autoHelper;
	AudioManager audiomanage;
	int ringerMode;

	public void onReceive(Context context, Intent intent) {
		Log.v(TAG, "Incoming Call BroadCast received...");
		this.context = context;
		blHelper = new BlackListHelper(context);
		wlHelper = new WhiteListHelper(context);
		ctHelper = new ContactHelper(context);
		mcHelper = new MissCallHelper(context);
		stHelper = new SettingHelper(context);
		autoHelper = new AutoDetectHelper(context);
		stConfig = stHelper.loadSetting();

		TelephonyManager telephony = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		MyPhoneStateListener mPhoneListener = new MyPhoneStateListener();
		telephony.listen(mPhoneListener, PhoneStateListener.LISTEN_CALL_STATE);
		
		audiomanage = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		ringerMode = audiomanage.getRingerMode();
		Log.v(TAG, "Receving....");

		Util.STATE++;
		Log.d("STATE", Util.STATE + "");
		try {
			Class c = Class.forName(telephony.getClass().getName());
			Method m = c.getDeclaredMethod("getITelephony");
			m.setAccessible(true);
			telephonyService = (ITelephony) m.invoke(telephony);

			if (Util.STATE == 1) {
				Bundle b = intent.getExtras();
				incomingNumber = b
						.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
				incomingNumber = incomingNumber.replaceAll("\\s", "");
				incomingNumber = Util.subStringPhoneNo(context,incomingNumber);
				Log.i("TEST", "Phone: " + incomingNumber);
				if(stConfig.enable == 1){
					if (blHelper.isExitInBlackList(incomingNumber)) {
						ContactModel ct = ctHelper.getContactByPhonenumber(incomingNumber);
						if (ct.blocktype == 1 || ct.blocktype == 3) {
							endCall();					
							saveMissCall(ct.name);
						}
					}
				}
				if(stConfig.enable == 2){
					if(wlHelper.isExitInWhiteList(incomingNumber)){
						//Cho phep duoc goi den
					}else{
						endCall();
						audiomanage.setRingerMode(AudioManager.RINGER_MODE_SILENT);
						saveMissCall(context.getString(R.string.out_of_list));
						telephonyService.cancelMissedCallsNotification();
					}
				}
				// Chan Nha Mang

				// viettel = 096, 097, 098, 0162, 0163, 0164, 0165, 0166, 0167,
				// 0168, 0169"
				// vina = "091, 094, 0123, 0124, 0125, 0127, 0129"
				// mobi = "090, 093, 0120, 0121, 0122, 0126, 0128"
				// vnmobi "092, 0188"
				// gmobi "0199, 099 "

				if (stConfig.viettel == 1 || stConfig.vina == 1
						|| stConfig.mobi == 1 || stConfig.vnmobile == 1
						|| stConfig.gmobile == 1) {
					String ba = incomingNumber.substring(0, 3);
					String bon = incomingNumber.substring(0, 4);
					// CHan mang VIETTEL
					if (stConfig.viettel == 1) {
						if (ba.equalsIgnoreCase("096")
								|| ba.equalsIgnoreCase("097")
								|| ba.equalsIgnoreCase("098")
								|| ba.equalsIgnoreCase("016")) {
							// Ket thuc cuoc goi
							endCall();
							saveMissCall(context.getString(R.string.lock_viettel));
						}
					} else if (stConfig.vina == 1) {
						if (ba.equalsIgnoreCase("091")
								|| ba.equalsIgnoreCase("094")
								|| bon.equalsIgnoreCase("0123")
								|| bon.equalsIgnoreCase("0124")
								|| bon.equalsIgnoreCase("0125")
								|| bon.equalsIgnoreCase("0127")
								|| bon.equalsIgnoreCase("0129")) {
							// CHan Mnag VINA
							endCall();
							saveMissCall(context.getString(R.string.lock_vina));
						}
					} else if (stConfig.mobi == 1) {
						if (ba.equalsIgnoreCase("090")
								|| ba.equalsIgnoreCase("093")
								|| bon.equalsIgnoreCase("0120")
								|| bon.equalsIgnoreCase("0121")
								|| bon.equalsIgnoreCase("0122")
								|| bon.equalsIgnoreCase("0126")
								|| bon.equalsIgnoreCase("0128")) {
							// CHan Mang MOBI
							endCall();
							saveMissCall(context.getString(R.string.lock_mobi));
						}
					} else if (stConfig.vnmobile == 1) {
						if (ba.equalsIgnoreCase("092")
								|| bon.equalsIgnoreCase("0188")) {
							// CHan mang VIETNAME MOBILE
							endCall();
							saveMissCall(context.getString(R.string.lock_vnmobile));
						}
					} else if (stConfig.gmobile == 1) {
						if (ba.equalsIgnoreCase("099")
								|| bon.equalsIgnoreCase("0199")) {
							// Chan mang GMOBILE
							endCall();
							saveMissCall(context.getString(R.string.lock_gmobile));
						}
					}

				}

				// Chan tuy chon bat dau vs ket thuc
				if (stConfig.startWith != 0 || stConfig.endWith != 0) {
					String startW = String.valueOf("0" + stConfig.startWith);
					String endW = String.valueOf(stConfig.endWith);

					if (stConfig.startWith != 0) {
						if (incomingNumber.substring(0, startW.length())
								.equals(startW)) {
							endCall();
							saveMissCall(context.getString(R.string.lock_first) + startW);
						}
					}
					if (stConfig.endWith != 0) {
						if (incomingNumber.substring(
								incomingNumber.length() - endW.length(),
								incomingNumber.length()).equals(endW)) {
							endCall();
							saveMissCall(context.getString(R.string.lock_last) + endW);
						}
					}

				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveMissCall(String name) {
		String id = Long.toHexString(Double.doubleToLongBits(Math.random()))
				.toUpperCase();

		String idCt = "";
		if (blHelper.isExitInBlackList(incomingNumber)) {
			idCt = ctHelper.getContactByPhonenumber(incomingNumber).id;

		}else{
			idCt = Long.toHexString(Double.doubleToLongBits(Math.random()))
					.toUpperCase();
			ctHelper.addContact(new ContactModel(idCt, incomingNumber , name, 10));
		}

		DateFormat date = new SimpleDateFormat("yyyy-MM-dd");
		Date getDate = new Date();

		DateFormat time = new SimpleDateFormat("HH:mm:ss");
		Date getTime = new Date();

		
		MissCallModel mc = new MissCallModel(id, name, incomingNumber, date.format(getDate),
				time.format(getTime));
		
		mcHelper.saveMissCall(mc);
	}

	class MyPhoneStateListener extends PhoneStateListener {
		public boolean RESET = true;

		public void onCallStateChanged(int state, String incomingNumber) {
			switch (state) {
			case TelephonyManager.CALL_STATE_RINGING:
//				Log.i("STATE", Util.STATE + "");
//				Log.i("Ringing", "Ring = " + incomingNumber);
				Util.PhoneNo = incomingNumber;

				if (Util.STATE == 1 || Util.STATE == 0) {
//					Log.i("Start Count Time", "Start Count Time =========== ");
					startSecOne = TimeUnit.MILLISECONDS.toSeconds(System
							.currentTimeMillis());

				}
				break;
			case TelephonyManager.CALL_STATE_OFFHOOK:
//				Log.i("Offhook", "Offhook - Util.PhoneNo = " + Util.PhoneNo);
				Util.Going = true;
				startSecTwo = TimeUnit.MILLISECONDS.toSeconds(System
						.currentTimeMillis());

				break;
			case TelephonyManager.CALL_STATE_IDLE:
//				Log.i("IDLE", "Idle - Util.PhoneNo = " + Util.PhoneNo);
//				Log.d("Going", "Going = " + Util.Going);
				if (Util.Going) {
					if (Util.STATE == 3) {
						endSecTwo = TimeUnit.MILLISECONDS.toSeconds(System
								.currentTimeMillis());
						longSecTwo = endSecTwo - startSecTwo;
//						Log.i("Start Count Time",
//								"END Count Time TWO =========== " + longSec);
						Util.STATE = 0;
						Util.Going = false;
						
						if(!blHelper.isExitInBlackList(Util.PhoneNo)){
							actionIt();
						}

					}
				} else {
					if (Util.STATE == 2) {
						endSecOne = TimeUnit.MILLISECONDS.toSeconds(System
								.currentTimeMillis());
						longSecOne = endSecOne - startSecOne;
//						Log.i("Start Count Time",
//								"END Count Time ONE =========== " + longSec);
						Util.STATE = 0;
						if(!blHelper.isExitInBlackList(Util.PhoneNo)){
							actionIt();
						}
					}
				}
				break;
			}
		}

	};

	public void actionIt() {
		String phoneNo = Util.PhoneNo;
		String id = "";
		boolean checkInContact = ctHelper.isExitThisNumber(phoneNo);
		if(checkInContact){
			id = ctHelper.getContactByPhonenumber(phoneNo).id;
		}
		boolean checkExit = autoHelper.isExitThisNumber(id);
		
		if (longSecOne < 10 || longSecTwo < 3) { // Kiem tra duration
			// Neu sdt Da Ton tai trong AutoDetect
			if (checkExit) {
				// Get so lan Quay Roi ra kiem tra
				int getCount = autoHelper
						.getNumberDisturb(id);
				Log.i("Exits Contact", getCount + "----");
				// Xet coi check da du A lan Quy Dinh chua, gia tri
				// Truyen qua la
				// getRadioAutoDetect

				if (stConfig.valueDetect != 0) {
					if (getCount == stConfig.valueDetect-1) {
						// Neu du roi thi get ID tu Contact va Add vao
						// BlackList | Xoa khoi NumberFakeCall
//						Log.i("Enought count", getCount + " times");
						String ctID = ctHelper
								.getIdContactByPhonenumber(phoneNo);
						blHelper.addBlackNumber(ctID);
						autoHelper.deleteNumberOfFakeCall(ctID);

					} else {
						// Neu chua du thi Tang Count len 1 roi INSERT
						// lai vao
						getCount+=1;
						Log.i("Count ++", getCount + " times");
						String idToAddCount = ctHelper
								.getIdContactByPhonenumber(phoneNo);
//						Log.i("Count ++", "IIIIDDDDD" + idToAddCount);
						autoHelper.insertCount(idToAddCount, getCount);
					}
				} 
//				else {
//					System.out.println("Radio button NONE");
//				}

			} else {
				// Neu chua co trong NumberFakeCall nhung la lan dau
				// quay roi thi Dua vao Contact va NumberFakeCall
				String IDPM = Long.toHexString(
						Double.doubleToLongBits(Math.random())).toUpperCase();
				isDisturb = new ContactModel(IDPM, phoneNo,
						context.getString(R.string.disturb),3);
				Log.i("New Contact", isDisturb.toString());
				ctHelper.addContact(isDisturb); 
				autoHelper.addNewNumber(isDisturb.id);
			}
		} else { // Thoi gian lon hon 8s
			// Neu SDT da ton tai trong NumberFakeCall ma duration >
			// 8s => Xoa khoi tinh nghi, xoa khoi Contact
			Log.i("Long > 10", "Long sec > 10 and Exit = TRUE");
			if (checkExit) {
				autoHelper.deleteNumberOfFakeCall(id);
				ctHelper.deleteContactFromNumberPhone(phoneNo);
			}
		}
	}
	
	public void endCall(){
		audiomanage.setRingerMode(AudioManager.RINGER_MODE_SILENT);
		telephonyService.endCall();
		audiomanage.setRingerMode(ringerMode);		
	}
	

}
