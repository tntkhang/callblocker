package tntkhang.controller;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import tntkhang.model.ContactModel;

import com.nonglamuniversity.disturbcall.R;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Utility {

	static Utility util;
	public static int saveRadioAutoDetect;
	public static int MODE = 0;
	public static String INCOMEMINGNO = "";
	public static boolean isSaveLog = false;
	public static boolean isBlackList = false;
	public static String actionUpDate = "com.action.updateUI";
	public static String actionUpdateBL = "com.action.updateBL";
	public static boolean isUSE = false;
	public static boolean isActive = true;
	public static int radioBtnValue = 0;

	public Utility() {
		System.out.println(saveRadioAutoDetect);
	}

	public static Utility getInstance() {
		if (util == null) {
			util = new Utility();
		}
		return util;
	}

	public int sysoutSaveRadio() {
		return saveRadioAutoDetect;
	}

	public static String getCurrentDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}

	public String getCurrentTime() {
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}

	public String getDateFromTimestamp(long time) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(time);
		return dateFormat.format(calendar.getTime());
	}

	public String getTimeFromTimestamp(long time) {
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(time);
		return dateFormat.format(calendar.getTime());
	}

	public void buildDialogNotice(Context context, String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setNeutralButton("Close", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	public ArrayList<ContactModel> loadAllContact(Activity activity) {
		Cursor cursor = null;
		String phoneNumber = null;
		String id = null;
		String name = null;
		String image = null;
		ContactModel contact = null;
		ArrayList<ContactModel> list = new ArrayList<ContactModel>();
		String selection = ContactsContract.Contacts.IN_VISIBLE_GROUP + " = '"
				+ ("1") + "'";
		try {
			cursor = activity.getContentResolver().query(
					ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
					selection, null, null);
			while (cursor.moveToNext()) {
				id = cursor
						.getString(cursor
								.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));

				phoneNumber = cursor
						.getString(cursor
								.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
				name = cursor
						.getString(cursor
								.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
				image = cursor
						.getString(cursor
								.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
				contact = new ContactModel(phoneNumber, name);
				list.add(contact);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return list;

	}

	public String isExitContactPhone(String phoneNumber,
			ArrayList<ContactModel> listContact) {
		for (ContactModel contact : listContact) {
			if (phoneNumber.equals(contact.phonenumber)) {
				return contact.name;
			}
		}
		return null;

	}


	public Date parseToDate(String date) {
		String dateArray[] = date.split("-");
		Date date2 = new Date(Integer.parseInt(dateArray[0]),
				Integer.parseInt(dateArray[1]), Integer.parseInt(dateArray[2]));
		return date2;

	}

	public Time parseToTime(String time) {
		String timeArray[] = time.split(":");
		Time time2 = new Time(Integer.parseInt(timeArray[0]),
				Integer.parseInt(timeArray[1]), Integer.parseInt(timeArray[2]));
		return time2;

	}
	
	

}
