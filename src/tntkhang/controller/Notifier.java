package tntkhang.controller;

import tntkhang.activity.MainActivity;

import com.nonglamuniversity.disturbcall.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Notifier {
	private final static int NOTIFICATION_ID = 1;

	private Context mContext;
	private NotificationManager mNotificationManager;
//	private SharedPreferences mSharedPreferences;

	public Notifier(Context context) {
		mContext = context;
		mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		/*mSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);*/
	}

	public void updateNotification(boolean flag, String text) {
		if (flag) {
			this.enableNotification(text);
		} else {
			this.disableNotification();
		}
	}

	private void enableNotification(String text) {
		// Intent to call to turn off AutoAnswer
		Intent notificationIntent = new Intent(mContext,
				MainActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0,
				notificationIntent, 0);

		// Create the notification
		Notification n = new Notification(R.drawable.my_app_small, null,
				0);
		n.flags |= Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;
		n.setLatestEventInfo(mContext,
				mContext.getString(R.string.app_name),
				text, pendingIntent);
		mNotificationManager.notify(NOTIFICATION_ID, n);
	}

	private void disableNotification() {
		mNotificationManager.cancel(NOTIFICATION_ID);
	}
}
