package tntkhang.controller;

import java.util.ArrayList;

import com.nonglamuniversity.disturbcall.R;
import com.nonglamuniversity.disturbcall.R.string;

import tntkhang.database.BlackListHelper;
import tntkhang.database.ContactHelper;
import tntkhang.database.MissCallHelper;
import tntkhang.database.MissSmsHelper;
import tntkhang.database.WhiteListHelper;
import tntkhang.model.ContactModel;
import tntkhang.model.StateContactModel;


import android.content.Context;
import android.util.Log;


public class Util {
	public static String PhoneNo = "";
	public static int STATE = 0;
	public static boolean Going = false;

	public ArrayList<StateContactModel> bubbleSort(ArrayList<StateContactModel> list) {
		int count = 0;
		for (int outer = 0; outer < list.size() - 1; outer++) {
			for (int inner = 0; inner < list.size() - outer - 1; inner++) {
				if (list.get(inner).ct.name
						.compareTo(list.get(inner + 1).ct.name) > 0) {
					swapEm(list, inner);
					count = count + 1;
				}
			}
		}
		return list;
	}

	private void swapEm(ArrayList<StateContactModel> list, int inner) {
		StateContactModel temp = list.get(inner);
		list.set(inner, list.get(inner + 1));
		list.set(inner + 1, temp);
	}

	public static void checkToDelete(Context context, String id) {
		BlackListHelper bl = new BlackListHelper(context);
		MissCallHelper mc = new MissCallHelper(context);
		MissSmsHelper ms = new MissSmsHelper(context);
		ContactHelper ct = new ContactHelper(context);

		if (bl.stillExist(id) || mc.stillExist(id) || ms.stillExist(id)) {

		} else {
			ct.deleteContactID(id);
		}

	}

	public static String subStringPhoneNo(Context context, String phoneNo) {
		String code = phoneNo.substring(0, 3);
		String result = "";
		if (code.equals(context.getString(R.string.code))) {
			result = phoneNo.replace(context.getString(R.string.code),
					context.getString(R.string.zero));
			return result;
		}else{
			return phoneNo;
		}
	}


}
