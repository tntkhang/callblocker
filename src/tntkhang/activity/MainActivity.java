package tntkhang.activity;

import com.nonglamuniversity.disturbcall.R;
import com.nonglamuniversity.disturbcall.R.anim;
import com.nonglamuniversity.disturbcall.R.id;
import com.nonglamuniversity.disturbcall.R.layout;
import com.nonglamuniversity.disturbcall.R.string;

import tntkhang.controller.Notifier;
import tntkhang.database.DatabaseHelper;
import tntkhang.database.SettingHelper;
import tntkhang.model.SettingConfigModel;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends Activity{

	Button btnBl, btnWl, btnMc, btnMs, btnSt;
	private LinearLayout layout_blacklist, layout_whitelist, layout_misscall, layout_misssms, layout_setting;
	private Notifier notify;
	SettingHelper stHelper;
	SettingConfigModel stConfig;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);

		DatabaseHelper db = new DatabaseHelper(MainActivity.this);
		System.out.println("Count table:  " + db.countTable());
		
		stHelper = new SettingHelper(this);
		stConfig = stHelper.loadSetting();
		if(stConfig.showicon == 1){
			notify = new Notifier(MainActivity.this);
			notify.updateNotification(true, getString(R.string.active_blacklist));
		}
		
		layout_blacklist = (LinearLayout) findViewById(R.id.layout_blacklist);
		layout_whitelist = (LinearLayout) findViewById(R.id.layout_whitelist);
		layout_misscall = (LinearLayout) findViewById(R.id.layout_misscall);
		layout_misssms = (LinearLayout) findViewById(R.id.layout_misssms);
		layout_setting = (LinearLayout) findViewById(R.id.layout_setting);
		layout_blacklist.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				switchActivity(BlackListActivity.class);
			}
		});
		layout_whitelist.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				switchActivity(WhiteListActivity.class);
			}
		});
		layout_misscall.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				switchActivity(MissCallActivity.class);
			}
		});
		layout_misssms.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				switchActivity(MissSmsActivity.class);
			}
		});
		layout_setting.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				switchActivity(SettingActivity.class);
			}
		});
		
	}
	public void switchActivity(Class cls){
		Intent i = new Intent(MainActivity.this, cls);
		startActivity(i);
		overridePendingTransition(R.anim.right_in, R.anim.left_out);
	}
}
