package tntkhang.activity;

import java.util.ArrayList;

import tntkhang.adapter.LockwordAdapter;
import tntkhang.database.LockWordHelper;
import tntkhang.model.LockwordModel;


import com.nonglamuniversity.disturbcall.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;
import android.widget.PopupMenu.OnMenuItemClickListener;

public class ListLockwordActivity extends Activity{
	
	Context context;
	LockWordHelper lwHelper;
	ArrayList<LockwordModel> lstLockwordData;
	LockwordAdapter lwAdapter;
	ListView lstViewKW;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.ms_manager_keyword);
		context = ListLockwordActivity.this;
		lstViewKW = (ListView)findViewById(R.id.lstViewKeyWord);
		lstLockwordData = new ArrayList<LockwordModel>();
		lwHelper = new LockWordHelper(context);
		
		resetListView();
		lwAdapter = new LockwordAdapter(context, lstLockwordData);
		lstViewKW.setAdapter(lwAdapter);
		
/*		btn_menu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				PopupMenu pop = new PopupMenu(ListLockwordScreen.this, v);
				pop.getMenuInflater().inflate(R.menu.popup_menu_ms, pop.getMenu());
				pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {
					
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						
						if (item.getTitle().equals(getString(R.string.setting))) {
							// startActivity Setting
							Intent i = new Intent(context, SettingScreen.class);
							startActivity(i);
						}else if (item.getTitle().equals(getString(R.string.addKey))) {
							//show input
							showInputNewKeyWord(); 
						}else if (item.getTitle().equals(getString(R.string.editKey))) {
						}
							return false;
					}
				});
				pop.show();
			}
		});*/
	}
	
	private void resetListView(){
		lstLockwordData = lwHelper.loadLockWord();
		lwAdapter = new LockwordAdapter(context,lstLockwordData);
		lwAdapter.notifyDataSetChanged();
		lstViewKW.setAdapter(lwAdapter);
	}
	
	private void showInputNewKeyWord(){
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.prompts_input_new_keyword, null);
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		//set layout to alert builder
		alertDialogBuilder.setView(promptsView);
		alertDialogBuilder.setTitle(getString(R.string.add_new_keyword));
		final EditText keywordInputName = (EditText)promptsView.findViewById(R.id.editTextDialogAddkeyWord);
		
		//set button OK and Cancel
		alertDialogBuilder.setCancelable(false).setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		}).setNegativeButton(getString(R.string.add), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String idPM = Long.toHexString(Double.doubleToLongBits(Math.random())).toLowerCase();
				String keyword = keywordInputName.getText().toString();
				lwHelper.addKeyWord(idPM,keyword);
				resetListView();
			}
		});
		
		
		//show input dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}
}
