package tntkhang.activity;

import java.util.ArrayList;

import tntkhang.adapter.DeleteContactAdapter;
import tntkhang.controller.Util;
import tntkhang.database.BlackListHelper;
import tntkhang.database.ContactHelper;
import tntkhang.model.ContactModel;
import tntkhang.model.StateContactModel;


import com.nonglamuniversity.disturbcall.R;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

/**
 * @author KHANGTRAN
 * @date May 18, 2013
 */

public class DeleteContactActivity extends Activity {
	ArrayList<StateContactModel> lstStateContact;
	ListView lstView;
	DeleteContactAdapter lstAdapter;
	ArrayList<ContactModel> lstSelected;
	ArrayList<ContactModel> lstContactData;
	BlackListHelper blHelper;
	ContactHelper ctHelper;
	Button checkAll;
	int click = 0;
	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.bl_delete_contact);

		lstView = (ListView) findViewById(R.id.lstViewGetContact);
		lstSelected = new ArrayList<ContactModel>();
		blHelper = new BlackListHelper(this);
		ctHelper = new ContactHelper(this);
		lstContactData = new ArrayList<ContactModel>();
		lstStateContact = new ArrayList<StateContactModel>();
		context = this;

		// resetListView();

		checkAll = (Button) findViewById(R.id.btnAllContact);

		checkAll.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				if (click % 2 == 1) {
					for (int i = 0; i < lstStateContact.size(); i++) {
						lstStateContact.get(i).state = false;
						lstSelected.clear();
					}
					click++;
				} else {
					for (int i = 0; i < lstStateContact.size(); i++) {
						lstStateContact.get(i).state = true;
						ContactModel c = lstStateContact.get(i).ct;
						lstSelected.add(c);
					}
					click++;
				}
				lstAdapter.notifyDataSetChanged();
			}
		});

		lstView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int pos,
					long id) {

				StateContactModel state = lstStateContact.get(pos);
				ContactModel c = state.ct;
				if (!state.state) {
					state.state = true;
					lstSelected.add(c);
				} else {
					state.state = false;
					lstSelected.remove(c);
				}
				lstAdapter.notifyDataSetChanged();
			}
		});

		Button add = (Button) findViewById(R.id.btnAddFromContact);
		Button cancel = (Button) findViewById(R.id.btnCancel);

		add.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (lstSelected.isEmpty()) {
					Toast.makeText(context, getString(R.string.list_null),
							Toast.LENGTH_SHORT).show();
				} else {
					new DeleteMultiContact().execute();
				}
			}
		});
		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();
		resetListView();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	private class DeleteMultiContact extends AsyncTask<Void, Void, Void> {

		/* Object */
		ProgressDialog dialog;

		/*
		 * Function name: onPreExecute Parameters: Void params Return: void
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = ProgressDialog.show(DeleteContactActivity.this, null,
					getString(R.string.deleting), true);
		}

		/*
		 * Function name: doInBackground Parameters: Void params Return: void
		 */
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			for (int i = 0; i < lstSelected.size(); i++) {
				blHelper.deleteContact(lstSelected.get(i).id);
				ctHelper.deleteContact(lstSelected.get(i).id);
				Util.checkToDelete(context, lstSelected.get(i).id);
				// Miss SMS xoa SMS cua sdt nay
				// Miss call xoa cuoc goi cua sdt nay

			}
			return null;
		}

		/*
		 * Function name: onPostExecute Parameters: Void result Return: void
		 */
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			lstAdapter = new DeleteContactAdapter(DeleteContactActivity.this,
					lstStateContact);
			lstView.setAdapter(lstAdapter);

			dialog.dismiss();
			DeleteContactActivity.this.finish();
		}
	}

	private void resetListView() {
		loadContact();
		lstAdapter = new DeleteContactAdapter(DeleteContactActivity.this,
				lstStateContact);
		lstAdapter.notifyDataSetChanged();
		lstView.setAdapter(lstAdapter);
	}

	public ArrayList<StateContactModel> loadContact() {
		lstContactData = blHelper.getAllListContactOfBlackList();
		for (int i = 0; i < lstContactData.size(); i++) {
			lstStateContact.add(new StateContactModel(lstContactData.get(i), false));
		}

		return lstStateContact;
	}

}
