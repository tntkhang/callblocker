package tntkhang.activity;

import java.util.ArrayList;

import tntkhang.adapter.BlackListAdapter;
import tntkhang.controller.CommonConstant;
import tntkhang.controller.Util;
import tntkhang.database.BlackListHelper;
import tntkhang.database.ContactHelper;
import tntkhang.database.MissCallHelper;
import tntkhang.model.ContactModel;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nonglamuniversity.disturbcall.R;

public class BlackListActivity extends Activity {
	static final int PICK_CONTACT_REQUEST = 1; // The request code
	ImageButton btn_menu;
	Context context;
	ContactHelper ctHelper;
	BlackListHelper blHelper;
	MissCallHelper mcHelper;
	ArrayList<ContactModel> lstContactData;
	BlackListAdapter blAdapter;
	ListView lstViewBL;
	public TextView tvNull;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.bl_main);
		context = BlackListActivity.this;
		lstViewBL = (ListView) findViewById(R.id.lstViewBL);
		tvNull = (TextView) findViewById(R.id.tvNull);
		lstContactData = new ArrayList<ContactModel>();
		ctHelper = new ContactHelper(context);
		blHelper = new BlackListHelper(context);
		mcHelper = new MissCallHelper(context);

		blAdapter = new BlackListAdapter(context, lstContactData, tvNull);
		lstViewBL.setAdapter(blAdapter);
		btn_menu = (ImageButton) findViewById(R.id.btnMenuBL);
		btn_menu.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				PopupMenu pop = new PopupMenu(BlackListActivity.this, v);
				pop.getMenuInflater().inflate(R.menu.popup_menu_bl,
						pop.getMenu());

				pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem item) {

						if (item.getTitle().equals(
								context.getString(R.string.setting))) {
							Intent i = new Intent(context,
									SettingActivity.class);
							startActivity(i);
						} else if (item.getTitle().equals(
								context.getString(R.string.addnew))) {
							// start pop
							showInputNewNumber();
						} else if (item.getTitle().equals(
								context.getString(R.string.addFromContact))) {
							Intent i = new Intent(context,
									PhoneContactActivity.class);
							// startActivity(i);

							pickContact();

						} else if (item.getTitle().equals(
								context.getString(R.string.delete))) {
							if (lstContactData.isEmpty()) {
								Toast.makeText(context,
										getString(R.string.list_null),
										Toast.LENGTH_SHORT).show();
							} else {
								startActivity(new Intent(
										BlackListActivity.this,
										DeleteContactActivity.class));
							}
						} else if (item.getTitle().equals(
								context.getString(R.string.addFromCallLogs))) {
							Intent i = new Intent(context,
									PhoneCallLogsActivity.class);
							startActivity(i);
						}
						return false;
					}
				});

				pop.show();
			}
		});
		lstViewBL.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int arg2,
					long arg3) {
				registerForContextMenu(lstViewBL);
				view.showContextMenu();

			}
		});

	}

	private void pickContact() {
		Intent pickContactIntent = new Intent(BlackListActivity.this, PhoneContactActivity.class);
		pickContactIntent.putExtra(CommonConstant.GET_CONTACT, PICK_CONTACT_REQUEST);
		startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    // Check which request it is that we're responding to
	    if (requestCode == PICK_CONTACT_REQUEST) {
	        // Make sure the request was successful
	        if (resultCode == RESULT_OK) {

	            Toast.makeText(context, "Success !", Toast.LENGTH_LONG).show();;
	        }
	    }
	}
	
	@Override
	protected void onStart() {
		super.onStart();

	}

	@Override
	protected void onResume() {
		super.onResume();
		resetListView();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenu.ContextMenuInfo menuInfo) {
		createMenu(R.menu.context_menu, menu, null);
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	private void createMenu(int menuID, ContextMenu menu, String title) {
		getMenuInflater().inflate(menuID, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();

		int id = info.position;
		ContactModel c = lstContactData.get(id);
		switch (item.getItemId()) {
		case R.id.context_menu_delete:
			blHelper.deleteContact(c.id);
			Util.checkToDelete(context, c.id);
			resetListView();
			return (true);
		case R.id.context_menu_edit:
			showInputEditNumber(c);
			return (true);
		case R.id.context_menu_view_miss_call:
			// chuyen sang man hinh Miss Call voi bo Loc la sdt vua chon
			Intent i = new Intent(BlackListActivity.this,
					MissCallActivity.class);
			i.putExtra("id", c.phonenumber);
			startActivity(i);
			return (true);
		case R.id.context_menu_view_miss_sms:
			// chuyen sang man hinh Miss SMS voi bo Loc la sdt vua chon
			Intent in = new Intent(BlackListActivity.this,
					MissSmsActivity.class);
			in.putExtra("id", c.phonenumber);
			startActivity(in);
			return (true);
		}
		return (super.onOptionsItemSelected(item));
	}

	private void resetListView() {
		loadContact();
		blAdapter = new BlackListAdapter(context, lstContactData, tvNull);
		if (lstContactData.isEmpty()) {
			tvNull.setVisibility(View.VISIBLE);
		}
		blAdapter.notifyDataSetChanged();
		lstViewBL.setAdapter(blAdapter);
	}

	private void showInputNewNumber() {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.prompts_input_new_contact, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		alertDialogBuilder.setTitle(getString(R.string.add_new_contact));

		final EditText userInputName = (EditText) promptsView
				.findViewById(R.id.editTextDialogUserInputName);
		final EditText userInputNumber = (EditText) promptsView
				.findViewById(R.id.editTextDialogUserInputNumberphone);
		final Spinner blocktype = (Spinner) promptsView
				.findViewById(R.id.spiBlockType);

		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						})
				.setNegativeButton(getString(R.string.add),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								String sdt = userInputNumber.getText()
										.toString();
								String name = userInputName.getText()
										.toString();
								String IDPM = Long.toHexString(
										Double.doubleToLongBits(Math.random()))
										.toUpperCase();
								int valueblocktype = 0;
								if (blocktype
										.getSelectedItem()
										.equals(context
												.getString(R.string.phone_and_sms))) {
									valueblocktype = 3;
								} else if (blocktype.getSelectedItem().equals(
										context.getString(R.string.only_sms))) {
									valueblocktype = 2;
								} else if (blocktype.getSelectedItem().equals(
										context.getString(R.string.only_phone))) {
									valueblocktype = 1;
								}
								if (sdt.equals("") || name.equals("")) {
									Toast.makeText(context,
											getString(R.string.error),
											Toast.LENGTH_SHORT).show();
								} else {
									ctHelper.addContact(new ContactModel(IDPM,
											sdt, name, valueblocktype));
									blHelper.addBlackNumber(IDPM);
									resetListView();
								}
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public ArrayList<ContactModel> loadContact() {
		lstContactData = blHelper.getAllListContactOfBlackList();
		return lstContactData;
	}

	private void showInputEditNumber(final ContactModel c) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.prompts_input_new_contact, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		alertDialogBuilder.setTitle(context.getString(R.string.editPhoneNo));

		final EditText userInputName = (EditText) promptsView
				.findViewById(R.id.editTextDialogUserInputName);
		userInputName.setText(c.name);
		final EditText userInputNumber = (EditText) promptsView
				.findViewById(R.id.editTextDialogUserInputNumberphone);
		userInputNumber.setText(c.phonenumber);

		final Spinner blocktype = (Spinner) promptsView
				.findViewById(R.id.spiBlockType);
		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						})
				.setNegativeButton(getString(R.string.add),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								String newName = userInputName.getText()
										.toString();
								String newPhoneNumber = userInputNumber
										.getText().toString();

								int valueblocktype = 0;
								if (blocktype
										.getSelectedItem()
										.equals(context
												.getString(R.string.phone_and_sms))) {
									valueblocktype = 3;
								} else if (blocktype.getSelectedItem().equals(
										context.getString(R.string.only_sms))) {
									valueblocktype = 2;
								} else if (blocktype.getSelectedItem().equals(
										context.getString(R.string.only_phone))) {
									valueblocktype = 1;
								}
								if (newName.equals("")
										|| newPhoneNumber.equals("")) {
									Toast.makeText(context,
											getString(R.string.error),
											Toast.LENGTH_SHORT).show();
									dialog.notify();
								} else {
									ContactModel ct = new ContactModel(c.id,
											newPhoneNumber, newName,
											valueblocktype);
									blHelper.updateContact(c.id, ct);
									resetListView();
								}

							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.left_in, R.anim.right_out);
	}
}
