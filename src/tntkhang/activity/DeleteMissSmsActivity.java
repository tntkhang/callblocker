package tntkhang.activity;

import java.util.ArrayList;

import tntkhang.adapter.DeleteMissSmsAdapter;
import tntkhang.controller.Util;
import tntkhang.database.ContactHelper;
import tntkhang.database.MissSmsHelper;
import tntkhang.model.ContactModel;
import tntkhang.model.MissSmsModel;
import tntkhang.model.StateMissSmsModel;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.nonglamuniversity.disturbcall.R;


/**
 * @author KHANGTRAN
 * @date Jun 6, 2013
 */

public class DeleteMissSmsActivity extends Activity {
	ArrayList<StateMissSmsModel> lstStateMissSms ;
	ListView lstView;
	DeleteMissSmsAdapter lstAdapter;
	ArrayList<MissSmsModel> lstSelected;
	ArrayList<MissSmsModel> lstMissSmsData;
	ContactHelper ctHelper;
	MissSmsHelper msHelper;
	ImageButton checkAll;
	int click = 0;
	Context context;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.ms_delete);

		lstView = (ListView) findViewById(R.id.lstViewDeleteMS);
		lstSelected = new ArrayList<MissSmsModel>();
		msHelper = new MissSmsHelper(this);
		ctHelper = new ContactHelper(this);
		lstMissSmsData = new ArrayList<MissSmsModel>();
		lstStateMissSms = new ArrayList<StateMissSmsModel>();
		context = this;
		
		checkAll = (ImageButton) findViewById(R.id.btnAllMS);

		checkAll.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				if (click % 2 == 1) {
					for (int i = 0; i < lstStateMissSms.size(); i++) {
						lstStateMissSms.get(i).state = false;
						MissSmsModel c = lstStateMissSms.get(i).ms;
						lstSelected.remove(c);
					}
					click++;
				} else {
					for (int i = 0; i < lstStateMissSms.size(); i++) {
						lstStateMissSms.get(i).state = true;
						MissSmsModel c = lstStateMissSms.get(i).ms;
						lstSelected.add(c);
					}
					click++;
				}
				lstAdapter.notifyDataSetChanged();
			}
		});

		lstView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int pos,
					long id) {

				StateMissSmsModel state = lstStateMissSms.get(pos);
				MissSmsModel c = state.ms;
					if (!state.state) {
						state.state = true;
						lstSelected.add(c);
					} else {
						state.state = false;
						lstSelected.remove(c);
					}
					lstAdapter.notifyDataSetChanged();
			}
		});

		Button add = (Button) findViewById(R.id.btnDeleteMS);
		Button cancel = (Button) findViewById(R.id.btnCancelMS);

		add.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
					new DeleteMultiContact().execute();
				}
		});
		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}
	@Override
	protected void onResume() {
		super.onResume();
		resetListView();
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	private class DeleteMultiContact extends AsyncTask<Void, Void, Void> {

		/* Object */
		ProgressDialog dialog;
		ContactModel ct;

		/*
		 * Function name: onPreExecute Parameters: Void params Return: void
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = ProgressDialog.show(DeleteMissSmsActivity.this, null,
					getString(R.string.deleting), true);
		}

		/*
		 * Function name: doInBackground Parameters: Void params Return: void
		 */
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			for(int i = 0 ; i < lstSelected.size(); i++){
				msHelper.remove(lstSelected.get(i).id);
				ct = ctHelper.getContactByPhonenumber(lstSelected.get(i).phoneno);
				Util.checkToDelete(context, ct.id);
			}
			return null;
		}

		/*
		 * Function name: onPostExecute Parameters: Void result Return: void
		 */
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			lstAdapter = new DeleteMissSmsAdapter(DeleteMissSmsActivity.this,
					lstStateMissSms);
			lstView.setAdapter(lstAdapter);

			dialog.dismiss();
			DeleteMissSmsActivity.this.finish();
		}
	}
	private void resetListView() {
		loadMissSms();
		lstAdapter = new DeleteMissSmsAdapter(DeleteMissSmsActivity.this,
				lstStateMissSms);
		lstAdapter.notifyDataSetChanged();
		lstView.setAdapter(lstAdapter);
	}
	public ArrayList<StateMissSmsModel> loadMissSms() {
		lstMissSmsData = msHelper.getAll();
		for(int i = 0 ; i < lstMissSmsData.size(); i++){
			lstStateMissSms.add(new StateMissSmsModel(lstMissSmsData.get(i), false));
		}

		return lstStateMissSms;
	}
}
