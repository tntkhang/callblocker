package tntkhang.activity;

import java.util.ArrayList;


import tntkhang.adapter.BlackListAdapter;
import tntkhang.adapter.MissCallAdapter;
import tntkhang.controller.Util;
import tntkhang.database.ContactHelper;
import tntkhang.database.MissCallHelper;
import tntkhang.model.ContactModel;
import tntkhang.model.MissCallModel;

import com.nonglamuniversity.disturbcall.R;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.PopupMenu.OnMenuItemClickListener;

public class MissCallActivity extends Activity{
	
	Context context;
	MissCallAdapter mcAdapter;
	MissCallHelper mcHelper;
	ContactHelper ctHelper;
	ListView lstView;
	ImageButton imgBtn;
	ArrayList<MissCallModel> lstMissCallData;
	public TextView tvNull;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.mc_main);
		
		context = MissCallActivity.this;
		mcHelper = new MissCallHelper(context);
		ctHelper = new ContactHelper(context);
		lstView = (ListView) findViewById(R.id.lstViewMc);
		lstMissCallData = new ArrayList<MissCallModel>();
		imgBtn = (ImageButton) findViewById(R.id.btnMenuMc);
		tvNull = (TextView) findViewById(R.id.tvNull);
		imgBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				PopupMenu pop = new PopupMenu(context, v);
				pop.getMenuInflater().inflate(R.menu.popup_menu_mc,
						pop.getMenu());

				pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem item) {
						if (item.getTitle().equals(getString(R.string.setting))) {
							Intent i = new Intent(context, SettingActivity.class);
							startActivity(i);
						}else if (item.getTitle().equals(getString(R.string.deleteMissCall))){
							startActivity(new Intent(context, DeleteMissCallActivity.class));
						}
						return false;
					}
				});

				pop.show();
			}
		});
		lstView.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				registerForContextMenu(lstView);
				view.showContextMenu();
			}
		});
		
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenu.ContextMenuInfo menuInfo) {
		createMenu(R.menu.mc_context_menu, menu, null);
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	private void createMenu(int menuID, ContextMenu menu, String title) {
		getMenuInflater().inflate(menuID, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();

		int id = info.position;
		MissCallModel c = lstMissCallData.get(id);
		ContactModel ct = ctHelper.getContactByPhonenumber(c.phoneno);
		switch (item.getItemId()) {
		case R.id.mc_context_menu_delete:
			mcHelper.deleteContact(c.id);
			Util.checkToDelete(context, ct.id);
			resetListView();
			return (true);
		case R.id.mc_context_menu_view:
			viewAllId(ct.phonenumber);
			return (true);
		}
		return (super.onOptionsItemSelected(item));
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		Bundle i = getIntent().getExtras();
		if(i!=null){
			String phoneno = i.getString("id");
			viewAllId(phoneno);
		}else{
			resetListView();
		}
	}
	
	public void viewAllId(String phoneNo){
		lstMissCallData = mcHelper.getAllRecordOfPhoneNo(phoneNo);
		mcAdapter = new MissCallAdapter(context, lstMissCallData, tvNull);
		mcAdapter.notifyDataSetChanged();
		lstView.setAdapter(mcAdapter);
	}
	
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	public void resetListView() {
		lstMissCallData = mcHelper.getAllRecord();
		mcAdapter = new MissCallAdapter(context, lstMissCallData, tvNull);
		if(lstMissCallData.isEmpty()){
			tvNull.setVisibility(View.VISIBLE);
		}
		mcAdapter.notifyDataSetChanged();
		lstView.setAdapter(mcAdapter);
	}
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.left_in, R.anim.right_out);
	}

}
