package tntkhang.activity;

import java.util.ArrayList;


import tntkhang.adapter.DeleteMissCallAdapter;
import tntkhang.controller.Util;
import tntkhang.database.ContactHelper;
import tntkhang.database.MissCallHelper;
import tntkhang.model.ContactModel;
import tntkhang.model.MissCallModel;
import tntkhang.model.StateMissCallModel;



import com.nonglamuniversity.disturbcall.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;


public class DeleteMissCallActivity extends Activity{
	
	ArrayList<StateMissCallModel> lstStateMissCall ;
	ListView lstView;
	DeleteMissCallAdapter lstAdapter;
	ArrayList<MissCallModel> lstSelected;
	ArrayList<MissCallModel> lstMissCallData;
	ContactHelper ctHelper;
	MissCallHelper mcHelper;
	ImageButton checkAll;
	int click = 0;
	Context context;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.mc_delete);

		lstView = (ListView) findViewById(R.id.lstViewDeleteMC);
		lstSelected = new ArrayList<MissCallModel>();
		mcHelper = new MissCallHelper(this);
		ctHelper = new ContactHelper(this);
		lstMissCallData = new ArrayList<MissCallModel>();
		lstStateMissCall = new ArrayList<StateMissCallModel>();
		context = this;
		
		checkAll = (ImageButton) findViewById(R.id.btnAllMC);

		checkAll.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				if (click % 2 == 1) {
					for (int i = 0; i < lstStateMissCall.size(); i++) {
						lstStateMissCall.get(i).state = false;
						MissCallModel c = lstStateMissCall.get(i).mc;
						lstSelected.remove(c);
					}
					click++;
				} else {
					for (int i = 0; i < lstStateMissCall.size(); i++) {
						lstStateMissCall.get(i).state = true;
						MissCallModel c = lstStateMissCall.get(i).mc;
						lstSelected.add(c);
					}
					click++;
				}
				lstAdapter.notifyDataSetChanged();
			}
		});

		lstView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int pos,
					long id) {

				StateMissCallModel state = lstStateMissCall.get(pos);
				MissCallModel c = state.mc;
					if (!state.state) { 
						state.state = true;
						lstSelected.add(c);
					} else {
						state.state = false; 
						lstSelected.remove(c);
					}
					lstAdapter.notifyDataSetChanged();
			}
		});

		Button add = (Button) findViewById(R.id.btnDeleteMC);
		Button cancel = (Button) findViewById(R.id.btnCancelMC);

		add.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
					new DeleteMultiContact().execute();
				}
		});
		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}
	@Override
	protected void onResume() {
		super.onResume();
		resetListView();
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	private class DeleteMultiContact extends AsyncTask<Void, Void, Void> {

		/* Object */
		ProgressDialog dialog;

		/*
		 * Function name: onPreExecute Parameters: Void params Return: void
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = ProgressDialog.show(DeleteMissCallActivity.this, null,
					getString(R.string.deleting), true);
		}

		/*
		 * Function name: doInBackground Parameters: Void params Return: void
		 */
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			for(int i = 0 ; i < lstSelected.size(); i++){
				mcHelper.deleteContact(lstSelected.get(i).id);
				ContactModel ct = ctHelper.getContactByPhonenumber(lstSelected.get(i).phoneno);
				Util.checkToDelete(context, ct.phonenumber);
			}
			return null;
		}

		/*
		 * Function name: onPostExecute Parameters: Void result Return: void
		 */
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			lstAdapter = new DeleteMissCallAdapter(DeleteMissCallActivity.this,
					lstStateMissCall);
			lstView.setAdapter(lstAdapter);

			dialog.dismiss();
			DeleteMissCallActivity.this.finish();
		}
	}
	private void resetListView() {
		loadMissCall();
		lstAdapter = new DeleteMissCallAdapter(DeleteMissCallActivity.this,
				lstStateMissCall);
		lstAdapter.notifyDataSetChanged();
		lstView.setAdapter(lstAdapter);
	}
	public ArrayList<StateMissCallModel> loadMissCall() {
		lstMissCallData = mcHelper.getAllRecord();
		for(int i = 0 ; i < lstMissCallData.size(); i++){
			lstStateMissCall.add(new StateMissCallModel(lstMissCallData.get(i), false));
		}

		return lstStateMissCall;
	}

}
