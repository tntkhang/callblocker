package tntkhang.activity;

import java.util.ArrayList;

import tntkhang.adapter.WhiteListAdapter;
import tntkhang.controller.CommonConstant;
import tntkhang.controller.Util;
import tntkhang.database.ContactHelper;
import tntkhang.database.WhiteListHelper;
import tntkhang.model.ContactModel;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.nonglamuniversity.disturbcall.R;

public class WhiteListActivity extends Activity {
	static final int PICK_CONTACT_REQUEST = 2; // The request code
	ListView lstView;
	ImageButton imgBtn;
	ContactHelper ctHelper;
	WhiteListHelper wlHelper;
	Context context;
	WhiteListAdapter wlAdapter;
	ArrayList<ContactModel> lstContactData;
	public TextView tvNull;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.wl_main);

		context = WhiteListActivity.this;
		lstView = (ListView) findViewById(R.id.lstViewWl);
		imgBtn = (ImageButton) findViewById(R.id.btnMenuWL);
		tvNull = (TextView) findViewById(R.id.tvNull);
		ctHelper = new ContactHelper(context);
		wlHelper = new WhiteListHelper(context);
		lstContactData = new ArrayList<ContactModel>();
		wlAdapter = new WhiteListAdapter(context, lstContactData, tvNull);

		imgBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				PopupMenu pop = new PopupMenu(WhiteListActivity.this, v);
				pop.getMenuInflater().inflate(R.menu.popup_menu_bl,
						pop.getMenu());

				pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem item) {
						if (item.getTitle().equals(getString(R.string.setting))) {
							// startActivity Setting
							Intent i = new Intent(context,
									SettingActivity.class);
							startActivity(i);
						} else if (item.getTitle().equals(
								getString(R.string.addnew))) {
							// start pop
							showInputNewNumber();
						} else if (item.getTitle().equals(
								getString(R.string.addFromContact))) {
							Intent i = new Intent(context,
									PhoneContactActivity.class);
							// startActivity(i);
							pickContact();
						} else if (item.getTitle().equals(
								getString(R.string.delete))) {
							if (lstContactData.isEmpty()) {
								Toast.makeText(context,
										getString(R.string.list_null),
										Toast.LENGTH_SHORT).show();
							} else {
								startActivity(new Intent(
										WhiteListActivity.this,
										DeleteContactActivity.class));
							}
						} else if (item.getTitle().equals(
								context.getString(R.string.addFromCallLogs))) {
							Intent i = new Intent(context,
									PhoneCallLogsActivity.class);
							startActivity(i);
						}
						return false;
					}
				});

				pop.show();
			}
		});

		lstView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				registerForContextMenu(lstView);
				view.showContextMenu();

			}
		});
	}

	private void pickContact() {
		Intent pickContactIntent = new Intent(WhiteListActivity.this,
				PhoneContactActivity.class);
		pickContactIntent.putExtra(CommonConstant.GET_CONTACT,
				PICK_CONTACT_REQUEST);
		startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Check which request it is that we're responding to
		if (requestCode == PICK_CONTACT_REQUEST) {
			// Make sure the request was successful
			if (resultCode == RESULT_OK) {
				Toast.makeText(context, "Success !", Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		resetListView();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenu.ContextMenuInfo menuInfo) {
		createMenu(R.menu.wl_context_menu, menu, null);
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	private void createMenu(int menuID, ContextMenu menu, String title) {
		getMenuInflater().inflate(menuID, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();

		int id = info.position;
		ContactModel c = lstContactData.get(id);
		switch (item.getItemId()) {
		case R.id.context_menu_delete:
			wlHelper.deleteContact(c.id);
			Util.checkToDelete(context, c.id);
			resetListView();
			return (true);
		case R.id.context_menu_edit:
			showInputEditNumber(c);
			return (true);
		}
		return (super.onOptionsItemSelected(item));
	}

	private void showInputEditNumber(final ContactModel c) {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.wl_promt_input, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		alertDialogBuilder.setTitle(context.getString(R.string.editPhoneNo));

		final EditText userInputName = (EditText) promptsView
				.findViewById(R.id.wleditTextDialogUserInputName);
		userInputName.setText(c.name);
		final EditText userInputNumber = (EditText) promptsView
				.findViewById(R.id.wleditTextDialogUserInputNumberphone);
		userInputNumber.setText(c.phonenumber);

		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						})
				.setNegativeButton(getString(R.string.add),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								String newName = userInputName.getText()
										.toString();
								String newPhoneNumber = userInputNumber
										.getText().toString();

								int valueblocktype = 10;
								if (newName.equals("")
										|| newPhoneNumber.equals("")) {
									Toast.makeText(context,
											getString(R.string.error),
											Toast.LENGTH_SHORT).show();
									dialog.notify();
								} else {
									ContactModel ct = new ContactModel(c.id,
											newPhoneNumber, newName,
											valueblocktype);
									wlHelper.updateContact(c.id, ct);

									resetListView();
								}
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	private void showInputNewNumber() {
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.wl_promt_input, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		alertDialogBuilder.setTitle(getString(R.string.add_new_contact));

		final EditText userInputName = (EditText) promptsView
				.findViewById(R.id.wleditTextDialogUserInputName);
		final EditText userInputNumber = (EditText) promptsView
				.findViewById(R.id.wleditTextDialogUserInputNumberphone);

		// set dialog message
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton(getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						})
				.setNegativeButton(getString(R.string.add),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								String sdt = userInputNumber.getText()
										.toString();
								String newName = userInputName.getText()
										.toString();
								String IDPM = Long.toHexString(
										Double.doubleToLongBits(Math.random()))
										.toUpperCase();
								int valueblocktype = 10;
								if (newName.equals("") || sdt.equals("")) {
									Toast.makeText(context,
											getString(R.string.error),
											Toast.LENGTH_SHORT).show();
									dialog.notify();
								} else {
									ctHelper.addContact(new ContactModel(IDPM,
											sdt, newName, valueblocktype));
									wlHelper.addNumber(IDPM);
									resetListView();
								}
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	private void resetListView() {
		lstContactData = wlHelper.getAllListContactOfWhiteList();
		wlAdapter = new WhiteListAdapter(context, lstContactData, tvNull);
		if (lstContactData.isEmpty()) {
			tvNull.setVisibility(View.VISIBLE);
		}
		lstView.setAdapter(wlAdapter);
	}

	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.left_in, R.anim.right_out);
	}
}
