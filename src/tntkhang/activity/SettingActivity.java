package tntkhang.activity;

import tntkhang.controller.CallBlock;
import tntkhang.controller.Notifier;
import tntkhang.database.SettingHelper;
import tntkhang.model.SettingConfigModel;

import com.nonglamuniversity.disturbcall.R;


import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ToggleButton;

public class SettingActivity extends Activity implements KeyListener {
	CheckBox chkboxWhitelist, chkboxBlacklist, chkboxShowIcon;
	ToggleButton tggBtnStartWith, tggBtnEndWith, tggBtnAutoDetect;
	EditText edtStartWith, edtEndWith, edtAutoDetectTimes;
	CheckBox chkviettel, chkvina, chkmobi, chkvietnamemobile, chkgmobile;
	SettingHelper stHelper;
	SettingConfigModel stConfig;
	MainActivity context;
	private ComponentName component;
	private static String KEY = "tntkhang";
	private Notifier notify;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.setting);
		component = new ComponentName(SettingActivity.this, CallBlock.class);
		notify = new Notifier(this);

		chkboxBlacklist = (CheckBox) findViewById(R.id.chkActiveBlackList);
		chkboxWhitelist = (CheckBox) findViewById(R.id.chkActiveWhiteList);
		chkboxShowIcon = (CheckBox) findViewById(R.id.chbShowIcon);
		tggBtnStartWith = (ToggleButton) findViewById(R.id.tggbtnStartWith);
		tggBtnEndWith = (ToggleButton) findViewById(R.id.tggbtnEndWith);
		tggBtnAutoDetect = (ToggleButton) findViewById(R.id.tggbtnAutoDetect);
		edtStartWith = (EditText) findViewById(R.id.edtStartWith);
		edtEndWith = (EditText) findViewById(R.id.edtEndWith);
		edtAutoDetectTimes = (EditText) findViewById(R.id.edtAutoDetectTimes);
		chkviettel = (CheckBox) findViewById(R.id.chkboxViettel);
		chkvina = (CheckBox) findViewById(R.id.chkboxVina);
		chkgmobile = (CheckBox) findViewById(R.id.chkboxGmobile);
		chkmobi = (CheckBox) findViewById(R.id.chkboxMobi);
		chkvietnamemobile = (CheckBox) findViewById(R.id.chkboxVietnamemobile);

		stHelper = new SettingHelper(this);

		stConfig = stHelper.loadSetting();
		 Log.i("TEST", "Load: "+stConfig.toString());
		// Log.i("EndWith", stConfig.endWith + "++");
		// Log.i("ValueDetect", stConfig.valueDetect + "++");
		applyLoadSettingConfig();

		chkboxBlacklist
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton arg0,
							boolean arg1) {
						if (chkboxBlacklist.isChecked()) {
							// active black list
							chkboxWhitelist.setChecked(false);
							if (chkboxShowIcon.isChecked()) {
								notify.updateNotification(
										true,
										getApplicationContext().getString(
												R.string.active_blacklist));
							}
							// showNotification();
							// SettingScreen.this.getPackageManager()
							// .setComponentEnabledSetting(component,
							// PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
							// PackageManager.DONT_KILL_APP);
						} else {
							// deactive black list
							if (chkboxShowIcon.isChecked()) {
								notify.updateNotification(true,
										getString(R.string.disable_app));
							}
							// SettingScreen.this.getPackageManager()
							// .setComponentEnabledSetting(component,
							// PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
							// PackageManager.DONT_KILL_APP);
							// mNM.cancel(NOTIFICATION);
						}
					}
				});
		chkboxWhitelist
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton arg0,
							boolean arg1) {
						if (chkboxWhitelist.isChecked()) {
							// active white list
							chkboxBlacklist.setChecked(false);
							if (chkboxShowIcon.isChecked()) {
								notify.updateNotification(
										true,
										getApplicationContext().getString(
												R.string.active_whitelist));
							}
						} else {
							// deactive white list
							if (chkboxShowIcon.isChecked()) {
								notify.updateNotification(true, getString(R.string.disable_app));
							}
						}
					}
				});
		chkboxShowIcon
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton arg0,
							boolean arg1) {
						if (chkboxShowIcon.isChecked()) {
							if (!chkboxBlacklist.isChecked()
									&& !chkboxWhitelist.isChecked()) {
								notify.updateNotification(
										true,
										getApplicationContext().getString(
												R.string.disable_app));
							} else if (chkboxBlacklist.isChecked()) {
								notify.updateNotification(
										true,
										getApplicationContext().getString(
												R.string.active_blacklist));
							} else if (chkboxWhitelist.isChecked()) {
								notify.updateNotification(
										true,
										getApplicationContext().getString(
												R.string.active_whitelist));
							}
						} else {
							notify.updateNotification(false, null);
						}
					}
				});

		tggBtnStartWith
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton arg0,
							boolean arg1) {
						if (tggBtnStartWith.isChecked()) {
							edtStartWith.setEnabled(true);
							edtStartWith.setText("");
							edtStartWith.setFocusable(true);
						} else {
							edtStartWith.setText(R.string.blockstartwith);
							edtStartWith.setEnabled(false);
						}
					}
				});

		tggBtnEndWith.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				if (tggBtnEndWith.isChecked()) {
					edtEndWith.setEnabled(true);
					edtEndWith.setText("");
					edtEndWith.setFocusable(true);
				} else {
					edtEndWith.setText(R.string.blockendwith);
					edtEndWith.setEnabled(false);
				}
			}
		});
		tggBtnAutoDetect
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (tggBtnAutoDetect.isChecked()) {
							edtAutoDetectTimes.setEnabled(true);
							edtAutoDetectTimes.setFocusable(true);
						} else {
							edtAutoDetectTimes.setText("");
							edtAutoDetectTimes.setEnabled(false);
						}
					}
				});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	public void applyLoadSettingConfig() {
		if (stConfig.enable == 0) {
			chkboxBlacklist.setChecked(false);
			chkboxWhitelist.setChecked(false);
		} else if (stConfig.enable == 1) {
			chkboxBlacklist.setChecked(true);
		} else {
			chkboxWhitelist.setChecked(true);
		}

		if (stConfig.showicon == 0) {
			chkboxShowIcon.setChecked(false);
		} else {
			chkboxShowIcon.setChecked(true);
		}

		if (stConfig.viettel == 1) {
			chkviettel.setChecked(true);
		} else {
			chkviettel.setChecked(false);
		}

		if (stConfig.vina == 1) {
			chkvina.setChecked(true);
		} else {
			chkvina.setChecked(false);
		}
		if (stConfig.mobi == 1) {
			chkmobi.setChecked(true);
		} else {
			chkmobi.setChecked(false);
		}

		if (stConfig.vnmobile == 1) {
			chkvietnamemobile.setChecked(true);
		} else {
			chkvietnamemobile.setChecked(false);
		}

		if (stConfig.gmobile == 1) {
			chkgmobile.setChecked(true);
		} else {
			chkgmobile.setChecked(false);
		}

		if (stConfig.startWith != 0) {
			tggBtnStartWith.setChecked(true);
			edtStartWith.setText("0" + stConfig.startWith);
			edtStartWith.setEnabled(true);
		} else {
			tggBtnStartWith.setChecked(false);
		}

		if (stConfig.endWith != 0) {
			tggBtnEndWith.setChecked(true);
			edtEndWith.setText(stConfig.endWith + "");
			edtEndWith.setEnabled(true);
		} else {
			tggBtnEndWith.setChecked(false);
			edtEndWith.setEnabled(false);
		}

		if (stConfig.valueDetect == 0) {
			edtAutoDetectTimes.setText("");
			edtAutoDetectTimes.setEnabled(false);
			tggBtnAutoDetect.setChecked(false);
		} else {
			edtAutoDetectTimes.setText(stConfig.valueDetect + "");
			edtAutoDetectTimes.setEnabled(true);
			tggBtnAutoDetect.setChecked(true);
		}
	}

	public void saveSettingConfig() {
		int enable = 0;
		if (chkboxBlacklist.isChecked()) {
			enable = 1;
		} else if (chkboxWhitelist.isChecked()) {
			enable = 2;
		}
		int showicon = 0;
		if (chkboxShowIcon.isChecked()) {
			showicon = 1;
		}

		int viettel = 0;
		int vina = 0;
		int mobi = 0;
		int vnmobi = 0;
		int gmobi = 0;

		if (chkviettel.isChecked()) {
			viettel = 1;
		}
		if (chkvina.isChecked()) {
			vina = 1;
		}
		if (chkmobi.isChecked()) {
			mobi = 1;
		}
		if (chkvietnamemobile.isChecked()) {
			vnmobi = 1;
		}
		if (chkgmobile.isChecked()) {
			gmobi = 1;
		}

		int startW = 0;
		if (tggBtnStartWith.isChecked()) {
			startW = Integer.parseInt(edtStartWith.getText().toString());
		}

		int endW = 0;
		if (tggBtnEndWith.isChecked()) {
			endW = Integer.parseInt(edtEndWith.getText().toString());
		}

		int autodetect = 0;
		if (tggBtnAutoDetect.isChecked()) {
			autodetect = Integer.parseInt(edtAutoDetectTimes.getText()
					.toString());
		}

		SettingConfigModel saveSetting = new SettingConfigModel(enable, showicon,
				viettel, vina, mobi, gmobi, vnmobi, startW, endW, autodetect);
		 Log.i("TEST", "Save: " + saveSetting.toString());
		stHelper.saveSetting(saveSetting);
	}

	@Override
	public void clearMetaKeyState(View arg0, Editable arg1, int arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getInputType() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean onKeyDown(View arg0, Editable arg1, int arg2, KeyEvent event) {
		return false;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
			switch (event.getAction()) {
			case KeyEvent.ACTION_DOWN:
				saveSettingConfig();
				finish();
				overridePendingTransition(R.anim.left_in, R.anim.right_out);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean onKeyOther(View arg0, Editable arg1, KeyEvent arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onKeyUp(View arg0, Editable arg1, int arg2, KeyEvent arg3) {
		// TODO Auto-generated method stub
		return false;
	}
}
