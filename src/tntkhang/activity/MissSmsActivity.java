package tntkhang.activity;

import java.util.ArrayList;


import tntkhang.adapter.MissCallAdapter;
import tntkhang.adapter.MissSmsAdapter;
import tntkhang.controller.Util;
import tntkhang.database.ContactHelper;
import tntkhang.database.LockWordHelper;
import tntkhang.database.MissSmsHelper;
import tntkhang.model.ContactModel;
import tntkhang.model.LockwordModel;
import tntkhang.model.MissSmsModel;

import com.nonglamuniversity.disturbcall.R;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class MissSmsActivity extends Activity {
	ImageButton btn_menu;
	Context context;
	ContactHelper ctHelper;
	MissSmsHelper msHelper;
	LockWordHelper lwHelper;
	ListView lstViewMS;
	ArrayList<MissSmsModel> listMissSmsData;
	MissSmsAdapter msAdapter;
	ArrayList<LockwordModel> lstLockwordData;
	public TextView tvNull;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.ms_main);
		
		context = MissSmsActivity.this;
		lstViewMS = (ListView)findViewById(R.id.lstViewMs);
		tvNull = (TextView) findViewById(R.id.tvNull);
		listMissSmsData = new ArrayList<MissSmsModel>();
		ctHelper = new ContactHelper(context);
		msHelper = new MissSmsHelper(context);
		lwHelper = new LockWordHelper(context);
		
		btn_menu = (ImageButton)findViewById(R.id.btnMenuMs);
		btn_menu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				PopupMenu pop = new PopupMenu(MissSmsActivity.this, v);
				pop.getMenuInflater().inflate(R.menu.popup_menu_ms, pop.getMenu());
				pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {
					
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						if (item.getTitle().equals(getString(R.string.setting))) {
							// startActivity Setting
							Intent i = new Intent(context, SettingActivity.class);
							startActivity(i); 
						}else if (item.getTitle().equals(getString(R.string.addKey))) {
							//show input
							showInputNewKeyWord();
						}else if (item.getTitle().equals(getString(R.string.editKey))) {
							
							lstLockwordData = new ArrayList<LockwordModel>();
							lwHelper = new LockWordHelper(context);
							lstLockwordData = lwHelper.loadLockWord();
							if(lstLockwordData.isEmpty()){
								Toast.makeText(context, getString(R.string.list_null), Toast.LENGTH_SHORT).show();
							}else{
								Intent i = new Intent(context, ListLockwordActivity.class);
								startActivity(i);
							}
						}else if (item.getTitle().equals(getString(R.string.deletesms))){
							Intent i = new Intent(context, DeleteMissSmsActivity.class);
							startActivity(i);
						}
							return false;
					}
				});
				pop.show();
			}
		});
		
		lstViewMS.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				registerForContextMenu(lstViewMS);
				arg1.showContextMenu();
			}
		});
	}
	@Override
	protected void onResume() {
		super.onResume();
		Bundle i = getIntent().getExtras();
		if(i!=null){
			String id = i.getString("id");
			viewAllPhoneNo(id);
		}else{
			resetListView();
		}
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	private void resetListView(){
		loadMissSMS();
		msAdapter = new MissSmsAdapter(context, listMissSmsData, tvNull);
		if(listMissSmsData.isEmpty()){
			tvNull.setVisibility(View.VISIBLE);
		}
		msAdapter.notifyDataSetChanged();
		lstViewMS.setAdapter(msAdapter);
	}
	
	public ArrayList<MissSmsModel> loadMissSMS(){
		listMissSmsData = msHelper.getAll();
		return listMissSmsData;
	}
	
	
	private void showInputNewKeyWord(){
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.prompts_input_new_keyword, null);
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		//set layout to alert builder
		alertDialogBuilder.setView(promptsView);
		alertDialogBuilder.setTitle(getString(R.string.add_new_keyword));
		final EditText keywordInputName = (EditText)promptsView.findViewById(R.id.editTextDialogAddkeyWord);
		
		//set button OK and Cancel
		alertDialogBuilder.setCancelable(false).setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		}).setNegativeButton(getString(R.string.add), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String idPM = Long.toHexString(Double.doubleToLongBits(Math.random())).toLowerCase();
				String keyword = keywordInputName.getText().toString();
				lwHelper.addKeyWord(idPM,keyword);
			}
		});
		
		
		//show input dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		createMenu(R.menu.ms_context_menu, menu, getString(R.string.option));
		super.onCreateContextMenu(menu, v, menuInfo);
	}
	private void createMenu(int menuID,ContextMenu menu,String title){
		getMenuInflater().inflate(menuID, menu);
		menu.setHeaderTitle(title);
	}
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
		int id = info.position;
		MissSmsModel missSms = listMissSmsData.get(id);
		ContactModel ct = ctHelper.getContactByPhonenumber(missSms.phoneno);
		switch (item.getItemId()) {
		case R.id.context_menu_delete_ms:
			msHelper.remove(missSms.id);
			Util.checkToDelete(context, ct.id);
			resetListView();
			return (true);
		case R.id.context_menu_info_ms:
			showInputInfoMissSMS(missSms);
			return (true);
		case R.id.context_menu_view_all_ms:
			viewAllPhoneNo(ct.phonenumber);
			return (true);
		}
				
		return super.onContextItemSelected(item);
	}
	
	public void viewAllPhoneNo(String phoneNo){
		listMissSmsData = msHelper.getAllRecordOfPhoneNo(phoneNo);
		msAdapter = new MissSmsAdapter(context, listMissSmsData, tvNull);
		msAdapter.notifyDataSetChanged();
		lstViewMS.setAdapter(msAdapter);
	}
	
	private void showInputInfoMissSMS(final MissSmsModel missSms){
		LayoutInflater li = LayoutInflater.from(context);
		View proView = li.inflate(R.layout.prompts_info_misssms, null);
		ContactModel ct = ctHelper.getContactByPhonenumber(missSms.phoneno);
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		
		alertDialogBuilder.setView(proView);
		alertDialogBuilder.setTitle(getString(R.string.detail_sms));
		
		final TextView txtPhone = (TextView)proView.findViewById(R.id.txtPhone);
		txtPhone.setText(ct.phonenumber);
		final TextView txtname = (TextView)proView.findViewById(R.id.txtname);
		txtname.setText(ct.name);
		final TextView txtContent = (TextView)proView.findViewById(R.id.txtContent);
		txtContent.setText(missSms.content);
		
		alertDialogBuilder.setCancelable(false).setPositiveButton(getString(R.string.add), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.left_in, R.anim.right_out);
	}
	
}
