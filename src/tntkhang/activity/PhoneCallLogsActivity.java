package tntkhang.activity;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import tntkhang.adapter.PhoneCallLogsAdapter;
import tntkhang.controller.Util;
import tntkhang.database.BlackListHelper;
import tntkhang.database.ContactHelper;
import tntkhang.model.CallLogModel;
import tntkhang.model.ContactModel;
import tntkhang.model.StateCallLogModel;

import com.nonglamuniversity.disturbcall.R;


import android.app.Activity;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

public class PhoneCallLogsActivity extends Activity {

	private ArrayList<StateCallLogModel> lstCallLogState;
	private ArrayList<ContactModel> lstSelected;
	private ListView lstView;
	private Button checkAll;
	private PhoneCallLogsAdapter callLogAdapter;
	BlackListHelper blHelper;
	ContactHelper ctHelper;
	int click = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.bl_get_phone_calllog);

		lstCallLogState = new ArrayList<StateCallLogModel>();
		lstSelected = new ArrayList<ContactModel>();
		blHelper = new BlackListHelper(this);
		ctHelper = new ContactHelper(this);

		lstView = (ListView) findViewById(R.id.lstViewGetCallLog);

		new ReadLogs().execute();

		checkAll = (Button) findViewById(R.id.btnAllCallLog);

		checkAll.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				if (click % 2 == 1) {
					for (int i = 0; i < lstCallLogState.size(); i++) {
						lstCallLogState.get(i).state = false;
						lstSelected.clear();
					}
					click++;
				} else {
					for (int i = 0; i < lstCallLogState.size(); i++) {
						lstCallLogState.get(i).state = true;
						StateCallLogModel state = lstCallLogState.get(i);
						CallLogModel c = state.calllog;
						ContactModel contact = new ContactModel(c.number, c.name);
						lstSelected.add(contact);
					}
					click++;
				}
				callLogAdapter.notifyDataSetChanged();
			}
		});

		lstView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int pos,
					long id) {
				StateCallLogModel state = lstCallLogState.get(pos);
				CallLogModel c = state.calllog;
				ContactModel contact = new ContactModel(c.number, c.name);

				if (blHelper.isExitInBlackList(contact.phonenumber)) {
					Toast.makeText(PhoneCallLogsActivity.this,
							getString(R.string.phone_exit), Toast.LENGTH_SHORT)
							.show();
				} else {
					if (!state.state) {
						state.state = true;
						lstSelected.add(contact);
					} else {
						state.state = false;
						lstSelected.remove(contact);
					}
					callLogAdapter.notifyDataSetChanged();
				}

			}
		});
		Button add = (Button) findViewById(R.id.btnAddFromCallLog);
		Button cancel = (Button) findViewById(R.id.btnCancelCallLog);

		add.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (lstSelected.size() == 0) {
					Toast.makeText(PhoneCallLogsActivity.this,
							getString(R.string.list_phone_empty),
							Toast.LENGTH_SHORT).show();
				} else {
					new AddCallLogs().execute();
					// }
				}
			}
		});
		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	private String getDuration(long milliseconds) {
		int seconds = (int) (milliseconds / 1000) % 60;
		int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
		int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);
		if (hours < 1)
			return minutes + ":" + seconds;
		return hours + ":" + minutes + ":" + seconds;
	}

	private String getDateTime(long milliseconds) {
		Date date = new Date(milliseconds);
		return DateFormat.getDateTimeInstance().format(new Date());
	}

	private void readCallLogs() {

		Cursor callLogCursor = getContentResolver().query(
				android.provider.CallLog.Calls.CONTENT_URI, null, null, null,
				android.provider.CallLog.Calls.DEFAULT_SORT_ORDER);
		if (callLogCursor != null) {
			while (callLogCursor.moveToNext()) {
				String id = callLogCursor.getString(callLogCursor
						.getColumnIndex(CallLog.Calls._ID));
				String name = callLogCursor.getString(callLogCursor
						.getColumnIndex(CallLog.Calls.CACHED_NAME));
				String cacheNumber = callLogCursor.getString(callLogCursor
						.getColumnIndex(CallLog.Calls.CACHED_NUMBER_LABEL));
				String number = callLogCursor.getString(callLogCursor
						.getColumnIndex(CallLog.Calls.NUMBER));
				long dateTimeMillis = callLogCursor.getLong(callLogCursor
						.getColumnIndex(CallLog.Calls.DATE));
				long durationMillis = callLogCursor.getLong(callLogCursor
						.getColumnIndex(CallLog.Calls.DURATION));
				int callType = callLogCursor.getInt(callLogCursor
						.getColumnIndex(CallLog.Calls.TYPE));

				String duration = getDuration(durationMillis * 1000);

				String dateString = getDateTime(dateTimeMillis);

				if (cacheNumber == null)
					cacheNumber = number;
				if (name == null)
					name = getString(R.string.no_name);

//				cacheNumber = number.replaceAll("\\s", "");
//				cacheNumber = Util.subStringPhoneNo(PhoneCallLogs.this,cacheNumber);
				CallLogModel callLogModel = new CallLogModel(name, cacheNumber,
						duration, dateString, callType);

				lstCallLogState.add(new StateCallLogModel(callLogModel, false));
			}
		}
		callLogCursor.close();
	}

	private class ReadLogs extends AsyncTask<Void, Void, Void> {

		/* Object */
		ProgressDialog dialog;

		/*
		 * Function name: onPreExecute Parameters: Void params Return: void
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = ProgressDialog.show(PhoneCallLogsActivity.this, null,
					getString(R.string.reading_history), true);
		}

		/*
		 * Function name: doInBackground Parameters: Void params Return: void
		 */
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			readCallLogs();
			return null;
		}

		/*
		 * Function name: onPostExecute Parameters: Void result Return: void
		 */
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			callLogAdapter = new PhoneCallLogsAdapter(PhoneCallLogsActivity.this,
					lstCallLogState);
			lstView.setAdapter(callLogAdapter);

			dialog.dismiss();
		}
	}

	private class AddCallLogs extends AsyncTask<Void, Void, Void> {

		/* Object */
		ProgressDialog dialog;

		/*
		 * Function name: onPreExecute Parameters: Void params Return: void
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = ProgressDialog.show(PhoneCallLogsActivity.this, null,
					getString(R.string.adding), true);
		}

		/*
		 * Function name: doInBackground Parameters: Void params Return: void
		 */
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			ctHelper.addFromContactToBL(lstSelected);
			return null;
		}

		/*
		 * Function name: onPostExecute Parameters: Void result Return: void
		 */
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			callLogAdapter = new PhoneCallLogsAdapter(PhoneCallLogsActivity.this,
					lstCallLogState);
			lstView.setAdapter(callLogAdapter);

			dialog.dismiss();

			PhoneCallLogsActivity.this.finish();
		}
	}

}
