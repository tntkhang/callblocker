package tntkhang.activity;

import java.util.ArrayList;

import tntkhang.adapter.PhoneContactAdapter;
import tntkhang.controller.Util;
import tntkhang.database.BlackListHelper;
import tntkhang.database.ContactHelper;
import tntkhang.model.ContactModel;
import tntkhang.model.StateContactModel;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.nonglamuniversity.disturbcall.R;

public class PhoneContactActivity extends Activity {
	ArrayList<StateContactModel> lstStateContact = new ArrayList<StateContactModel>();
	ListView lstView;
	PhoneContactAdapter lstAdapter;
	ArrayList<ContactModel> lstSelected;
	BlackListHelper blHelper;
	ContactHelper ctHelper;
	Button checkAll;
	int click = 0;
	
	int key =0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.bl_get_phone_contact);

		lstView = (ListView) findViewById(R.id.lstViewGetContact);
		lstSelected = new ArrayList<ContactModel>();
		blHelper = new BlackListHelper(this);
		ctHelper = new ContactHelper(this);

		new ReadContact().execute();

		checkAll = (Button) findViewById(R.id.btnAllContact);

		checkAll.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				if (click % 2 == 1) {
					for (int i = 0; i < lstStateContact.size(); i++) {
						lstStateContact.get(i).state = false;
						lstSelected.clear();
					}
					click++;
				} else {
					for (int i = 0; i < lstStateContact.size(); i++) {
						lstStateContact.get(i).state = true;
						StateContactModel state = lstStateContact.get(i);
						ContactModel c = state.ct;
						lstSelected.add(c);
					}
					click++;
				}
				lstAdapter.notifyDataSetChanged();
			}
		});

		lstView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int pos,
					long id) {

				StateContactModel state = lstStateContact.get(pos);
				ContactModel c = state.ct;
				if (blHelper.isExitInBlackList(c.phonenumber)) {
					Toast.makeText(PhoneContactActivity.this,
							getString(R.string.phone_exit), Toast.LENGTH_SHORT)
							.show();
				} else {
					if (!state.state) {
						state.state = true;
						lstSelected.add(c);
					} else {
						state.state = false;
						lstSelected.remove(c);
					}
					lstAdapter.notifyDataSetChanged();
				}

			}
		});

		Button add = (Button) findViewById(R.id.btnAddFromContact);
		Button cancel = (Button) findViewById(R.id.btnCancel);

		add.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (lstSelected.size() == 0) {
					Toast.makeText(PhoneContactActivity.this,
							getString(R.string.list_phone_empty),
							Toast.LENGTH_SHORT).show();
				} else {
					new AddMultiContact().execute();
				}
			}
		});
		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();
		
		Bundle b = getIntent().getExtras();
		
		key = b.getInt("key");
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	public void load() {
		Cursor phones = getContentResolver().query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
				null, null);
		while (phones.moveToNext()) {

			String name = phones
					.getString(phones
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

			String phoneNumber = phones
					.getString(phones
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
			phoneNumber = phoneNumber.replaceAll("\\s","");
			phoneNumber = Util.subStringPhoneNo(PhoneContactActivity.this, phoneNumber);
			ContactModel objContact = new ContactModel(phoneNumber, name);
			lstStateContact.add(new StateContactModel(objContact, false));
		}
		for (int i = 0; i < lstStateContact.size(); i++) {
			for (int j = lstStateContact.size() - 1; j > i; j--) {
				if (lstStateContact.get(i).ct.phonenumber
						.equals(lstStateContact.get(j).ct.phonenumber)) {
					lstStateContact.remove(j);
				}
			}
		}
		phones.close();
		if (null != lstStateContact && lstStateContact.size() != 0) {
			Util u = new Util();
			u.bubbleSort(lstStateContact);

		} else {
			/*Toast.makeText(PhoneContactActivity.this,
					getString(R.string.phone_contact_empty), Toast.LENGTH_SHORT)
					.show();*/
		}

	}

	private class ReadContact extends AsyncTask<Void, Void, Void> {

		/* Object */
		ProgressDialog dialog;

		/*
		 * Function name: onPreExecute Parameters: Void params Return: void
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = ProgressDialog.show(PhoneContactActivity.this, null,
					getString(R.string.reading_contact), true);
		}

		/*
		 * Function name: doInBackground Parameters: Void params Return: void
		 */
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			load();
			return null;
		}

		/*
		 * Function name: onPostExecute Parameters: Void result Return: void
		 */
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			lstAdapter = new PhoneContactAdapter(PhoneContactActivity.this,
					lstStateContact);
			lstView.setAdapter(lstAdapter);

			dialog.dismiss();
		}
	}

	private class AddMultiContact extends AsyncTask<Void, Void, Void> {

		/* Object */
		ProgressDialog dialog;

		/*
		 * Function name: onPreExecute Parameters: Void params Return: void
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = ProgressDialog.show(PhoneContactActivity.this, null,
					getString(R.string.adding), true);
		}

		/*
		 * Function name: doInBackground Parameters: Void params Return: void
		 */
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			if(key == 1){
				ctHelper.addFromContactToBL(lstSelected);
			}else if(key == 2){
				ctHelper.addFromContactToWL(lstSelected);
			}
			return null;
		}

		/*
		 * Function name: onPostExecute Parameters: Void result Return: void
		 */
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			lstAdapter = new PhoneContactAdapter(PhoneContactActivity.this,
					lstStateContact);
			lstView.setAdapter(lstAdapter);

			dialog.dismiss();
			
			Intent rs = null;
			if(key ==1){
				rs = new Intent(PhoneContactActivity.this, BlackListActivity.class);
			}else if(key == 2){
				rs = new Intent(PhoneContactActivity.this, WhiteListActivity.class);
			}
			
			setResult(Activity.RESULT_OK, rs);
			PhoneContactActivity.this.finish();
		}
	}

}
