package tntkhang.model;

/*
 * Author: Khang Tran*/
public class ContactModel {


	public String id;
	public String phonenumber;
	public String name;
	public int blocktype;
	
	public ContactModel() {
	}
	
	public ContactModel(String id, String phonenumber, String name, int blocktype) {
		super();
		this.id = id;
		this.phonenumber = phonenumber;
		this.name = name;
		this.blocktype = blocktype;
	}
	public ContactModel(String phonenumber, String name) {
		super();
		this.phonenumber = phonenumber;
		this.name = name;
	}

	@Override
	public String toString() {
		return "Contact [id=" + id + ", phonenumber=" + phonenumber + ", name="
				+ name + ", blocktype=" + blocktype + "]";
	}
	


}
