package tntkhang.model;

public class CallLogModel {

	public String name;
	public String number;
	public String duration;
	public String date;
	public int type;

	public CallLogModel(String name, String number) {
		this.name = name;
		this.number = number;
	}

	public CallLogModel(String name, String number, String duration,
			String date, int type) {
		this.name = name;
		this.number = number;
		this.duration = duration;
		this.date = date;
		this.type = type;
	}
}
