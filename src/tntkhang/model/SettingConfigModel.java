package tntkhang.model;

public class SettingConfigModel {
	public String id;
	public int enable;
	public int showicon;
	public int viettel;
	public int vina;
	public int mobi;
	public int gmobile;
	public int vnmobile;
	public int startWith;
	public int endWith;
	public int valueDetect;
	
	public SettingConfigModel(int enable, int showicon, int viettel, int vina, int mobi,
			int gmobile, int vnmobile, int startWith, int endWith, int valueDetect) {
		super();
		this.enable = enable;
		this.showicon = showicon;
		this.viettel = viettel;
		this.vina = vina;
		this.mobi = mobi;
		this.gmobile = gmobile;
		this.vnmobile = vnmobile;
		this.startWith = startWith;
		this.endWith = endWith;
		this.valueDetect = valueDetect;
		
	}
	public SettingConfigModel(String id,int showicon, int enable, int viettel, int vina,
			int mobi, int gmobile, int vnmobile, int startWith,
			int endWith, int valueDetect) {
		super();
		this.id = id;
		this.enable = enable;
		this.showicon = showicon;
		this.viettel = viettel;
		this.vina = vina;
		this.mobi = mobi;
		this.gmobile = gmobile;
		this.vnmobile = vnmobile;
		this.startWith = startWith;
		this.endWith = endWith;
		this.valueDetect = valueDetect;
	}
	@Override
	public String toString() {
		return "SettingConfig [id=" + id + ", enable=" + enable + ", showicon="
				+ showicon + ", viettel=" + viettel + ", vina=" + vina
				+ ", mobi=" + mobi + ", gmobile=" + gmobile + ", vnmobile="
				+ vnmobile + ", startWith=" + startWith + ", endWith="
				+ endWith + ", valueDetect=" + valueDetect + "]";
	}
	
}
