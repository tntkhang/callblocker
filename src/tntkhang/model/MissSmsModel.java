package tntkhang.model;

public class MissSmsModel {
	public String id;
	public String name;
	public String phoneno;
	public String content;
	public String date;
	public String time;
	public MissSmsModel(String id, String name, String phoneno, String content,
			String date, String time) {
		super();
		this.id = id;
		this.name = name;
		this.phoneno = phoneno;
		this.content = content;
		this.date = date;
		this.time = time;
	}

}

