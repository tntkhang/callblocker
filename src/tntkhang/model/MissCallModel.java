package tntkhang.model;

/**
 * @author KHANGTRAN
 * @date May 30, 2013
 */

public class MissCallModel {
	public String id;
	public String name;
	public String phoneno;
	public String date;
	public String time;
	public MissCallModel(String id, String name, String phoneno, String date,
			String time) {
		super();
		this.id = id;
		this.name = name;
		this.phoneno = phoneno;
		this.date = date;
		this.time = time;
	}
	
	
}
