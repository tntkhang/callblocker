package tntkhang.adapter;

import java.util.ArrayList;

import tntkhang.database.ContactHelper;
import tntkhang.model.ContactModel;
import tntkhang.model.MissCallModel;
import tntkhang.model.StateMissCallModel;


import com.nonglamuniversity.disturbcall.R;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class DeleteMissCallAdapter extends BaseAdapter{

	private Context context;
	private ArrayList<StateMissCallModel> lstContact;
	private ContactHelper ctHelper;

	public DeleteMissCallAdapter(Context context, ArrayList<StateMissCallModel> lstContact) {
		super();
		this.context = context;
		this.lstContact = lstContact;
		ctHelper = new ContactHelper(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lstContact.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		View v = convertView;

		if (v == null) {
			LayoutInflater lay = LayoutInflater.from(context);
			v = lay.inflate(R.layout.mc_main_row, null);
		}

		
		if (lstContact.get(pos).state) {
			// if True -> set background CYAN
			v.setBackgroundColor(Color.CYAN);
		} else {
			// if false -> set background TRANFERANT
			v.setBackgroundColor(Color.TRANSPARENT);
		}

//		if(lstContact.size() != 0){
//			
//			Log.i("S","Size " + lstContact.size());
//			Contact ct = ctHelper.getContactById(lstContact.get(pos).mc.idcontact);
//		}
		TextView txtName = (TextView) v.findViewById(R.id.txtNameMC);
		txtName.setText(lstContact.get(pos).mc.name);
		
		TextView txtPhone = (TextView) v.findViewById(R.id.txtPhoneMC);
		txtPhone.setText(lstContact.get(pos).mc.phoneno);

		TextView txtDay = (TextView) v.findViewById(R.id.txtDayMC);
		txtDay.setText(lstContact.get(pos).mc.date);

		TextView txtTime = (TextView) v.findViewById(R.id.txtTimeMC);
		txtTime.setText(lstContact.get(pos).mc.time);

		return v;
	}

}
