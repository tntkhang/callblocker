package tntkhang.adapter;

import java.util.ArrayList;

import tntkhang.model.CallLogModel;
import tntkhang.model.StateCallLogModel;

import com.nonglamuniversity.disturbcall.R;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PhoneCallLogsAdapter extends BaseAdapter{
	
	private Context mContext;
	private ArrayList<StateCallLogModel> lstCallLog;
	
	public PhoneCallLogsAdapter(Context mContext,
			ArrayList<StateCallLogModel> lstCallLog) {
		super();
		this.mContext = mContext;
		this.lstCallLog = lstCallLog;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lstCallLog.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater lat = LayoutInflater.from(mContext);
			v = lat.inflate(R.layout.bl_get_phone_calllog_row, null);
		}
		if (lstCallLog.get(position).state) {
			// if True -> set background CYAN
			v.setBackgroundColor(Color.CYAN);
		} else {
			// if false -> set background TRANFERANT
			v.setBackgroundColor(Color.TRANSPARENT);
		}
		
		CallLogModel callLog = lstCallLog.get(position).calllog;
		
		TextView name = (TextView) v.findViewById(R.id.txtCallLogName);
		name.setText(callLog.name);
		
		TextView phoneno = (TextView) v.findViewById(R.id.txtCallLogPhoneNo);
		phoneno.setText(callLog.number);
		
		TextView date = (TextView) v.findViewById(R.id.txtCallLogDate);
		date.setText(callLog.date);
		
		TextView duration = (TextView) v.findViewById(R.id.txtCallLogDuration);
		duration.setText(callLog.duration + " s");
		
		TextView type = (TextView) v.findViewById(R.id.txtCallLogType);
		if (callLog.type == 1) {
			type.setText(mContext.getString(R.string.incoming_call));
		} else if (callLog.type == 2) {
			type.setText(mContext.getString(R.string.outgoing_call));
		} else {
			type.setText(mContext.getString(R.string.phone_miss_call));
		}
		
		return v;
	}

}
