package tntkhang.adapter;

import java.util.ArrayList;

import tntkhang.model.StateContactModel;

import com.nonglamuniversity.disturbcall.R;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PhoneContactAdapter extends BaseAdapter {

	private Context mContext;
	private ArrayList<StateContactModel> lstContact;

	public PhoneContactAdapter(Context mContext,
			ArrayList<StateContactModel> lstContact) {
		super();
		this.mContext = mContext;
		this.lstContact = lstContact;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lstContact.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater lat = LayoutInflater.from(mContext);
			v = lat.inflate(R.layout.bl_get_phone_contact_row, null);
		}

		if (lstContact.get(position).state) {
			// if True -> set background CYAN
			v.setBackgroundColor(Color.CYAN);
		} else {
			// if false -> set background TRANFERANT
			v.setBackgroundColor(Color.TRANSPARENT);
		}
		
		TextView name = (TextView) v.findViewById(R.id.txtBLGetContactName);
		name.setText(lstContact.get(position).ct.name);
		TextView no = (TextView) v.findViewById(R.id.txtBLGetContactNumber);
		no.setText(lstContact.get(position).ct.phonenumber);


		return v;
	}

}
