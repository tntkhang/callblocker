package tntkhang.adapter;

import java.util.ArrayList;

import tntkhang.database.ContactHelper;
import tntkhang.database.MissCallHelper;
import tntkhang.model.ContactModel;
import tntkhang.model.MissCallModel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nonglamuniversity.disturbcall.R;


public class MissCallAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<MissCallModel> lstContact;
	private ContactHelper ctHelper;
	private TextView tvNull;

	public MissCallAdapter(Context context, ArrayList<MissCallModel> lstContact, TextView tvNull) {
		super();
		this.context = context;
		this.lstContact = lstContact;
		ctHelper = new ContactHelper(context);
		this.tvNull = tvNull;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lstContact.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		View v = convertView;

		if (v == null) {
			LayoutInflater lay = LayoutInflater.from(context);
			v = lay.inflate(R.layout.mc_main_row, null);
		}
		if(!lstContact.isEmpty()){
			tvNull.setVisibility(View.GONE);
		}

		TextView txtName = (TextView) v.findViewById(R.id.txtNameMC);
		txtName.setText(lstContact.get(pos).name);

		TextView txtPhone = (TextView) v.findViewById(R.id.txtPhoneMC);
		txtPhone.setText(lstContact.get(pos).phoneno);

		TextView txtDay = (TextView) v.findViewById(R.id.txtDayMC);
		txtDay.setText(lstContact.get(pos).date);

		TextView txtTime = (TextView) v.findViewById(R.id.txtTimeMC);
		txtTime.setText(lstContact.get(pos).time);

		return v;
	}

}
