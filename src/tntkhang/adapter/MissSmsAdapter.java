package tntkhang.adapter;

import java.util.ArrayList;

import tntkhang.database.ContactHelper;
import tntkhang.database.MissSmsHelper;
import tntkhang.model.ContactModel;
import tntkhang.model.MissSmsModel;

import com.nonglamuniversity.disturbcall.R;



import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MissSmsAdapter extends BaseAdapter{
	private Context mContext;
	private ArrayList<MissSmsModel> listMissSmsData;
	private ContactHelper ctHelper;
	private TextView tvNull;
	
	public MissSmsAdapter(Context mContext, ArrayList<MissSmsModel> listMissSmsData, TextView tvNull) {
		super();
		this.mContext = mContext;
		this.listMissSmsData = listMissSmsData;
		ctHelper = new ContactHelper(mContext);
		this.tvNull = tvNull;
	}

	@Override
	public int getCount() {
		return listMissSmsData.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int pos, View convertView, ViewGroup parent) {
		View v = convertView;
		if(v==null){
			LayoutInflater laInflater = LayoutInflater.from(mContext);
			v = laInflater.inflate(R.layout.ms_main_row, null);
			
		}
		if(!listMissSmsData.isEmpty()){
			tvNull.setVisibility(View.GONE);
		}
		TextView phonenum = (TextView)v.findViewById(R.id.txtPhoneNoMS);
		phonenum.setText(listMissSmsData.get(pos).phoneno);
		TextView name = (TextView)v.findViewById(R.id.txtNameMS);
		name.setText(listMissSmsData.get(pos).name);
		TextView date = (TextView)v.findViewById(R.id.textDate);
		date.setText(listMissSmsData.get(pos).date);
		TextView time = (TextView)v.findViewById(R.id.txtTime);
		time.setText(listMissSmsData.get(pos).time);
		
		return v;
	}

}
