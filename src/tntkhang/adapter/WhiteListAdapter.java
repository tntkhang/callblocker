package tntkhang.adapter;

import java.util.ArrayList;

import tntkhang.model.ContactModel;

import com.nonglamuniversity.disturbcall.R;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * @author KHANGTRAN
 * @date Jun 1, 2013
 */

public class WhiteListAdapter extends BaseAdapter{
	
	private Context context;
	private ArrayList<ContactModel> lstContact;
	private TextView tvNull;
	

	public WhiteListAdapter(Context context, ArrayList<ContactModel> lstContact,TextView tvNull) {
		super();
		this.context = context;
		this.lstContact = lstContact;
		this.tvNull = tvNull;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lstContact.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		View v = convertView;
		
		if(v == null){
			LayoutInflater lay = LayoutInflater.from(context);
			v = lay.inflate(R.layout.wl_main_row, null);
		}
		if(!lstContact.isEmpty()){
			tvNull.setVisibility(View.GONE);
		}
		
		TextView txtPhone = (TextView) v.findViewById(R.id.txtPhoneNoWL);
		txtPhone.setText(lstContact.get(pos).phonenumber);
		
		TextView txtName = (TextView) v.findViewById(R.id.txtNameWL);
		txtName.setText(lstContact.get(pos).name);
		
		return v;
	}


}
