package tntkhang.adapter;

import java.util.ArrayList;

import tntkhang.model.ContactModel;

import com.nonglamuniversity.disturbcall.R;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class BlackListAdapter extends BaseAdapter {
	private Context mContext;
	private ArrayList<ContactModel> lstContactData;
	private TextView tvNull;

	public BlackListAdapter(Context mContext, ArrayList<ContactModel> lstContactData, TextView tvNull) {
		super();
		this.mContext = mContext;
		this.lstContactData = lstContactData;
		this.tvNull = tvNull;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lstContactData.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v = convertView;
		if (v == null) {
			LayoutInflater latInflater = LayoutInflater.from(mContext);
			v = latInflater.inflate(R.layout.bl_main_row, null);
		}
		if(!lstContactData.isEmpty()){
			tvNull.setVisibility(View.GONE);
		}
		
		TextView phoneNumber = (TextView) v.findViewById(R.id.txtPhoneNoBL);
		phoneNumber.setText(lstContactData.get(position).phonenumber);

		TextView name = (TextView) v.findViewById(R.id.txtNameBL);
		name.setText(lstContactData.get(position).name);

		TextView blocktype = (TextView) v.findViewById(R.id.txtBlockType);
		if (lstContactData.get(position).blocktype == 3) {
			blocktype.setText(mContext.getString(R.string.phone_and_sms));
		} else if (lstContactData.get(position).blocktype == 2) {
			blocktype.setText(mContext.getString(R.string.only_sms));
		} else if (lstContactData.get(position).blocktype == 1) {
			blocktype.setText(mContext.getString(R.string.only_phone));
		}
		return v;
	}

}
