package tntkhang.adapter;

import java.util.ArrayList;

import tntkhang.database.ContactHelper;
import tntkhang.database.MissSmsHelper;
import tntkhang.model.ContactModel;
import tntkhang.model.MissSmsModel;
import tntkhang.model.StateMissSmsModel;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.nonglamuniversity.disturbcall.R;


/**
 * @author KHANGTRAN
 * @date Jun 6, 2013
 */

public class DeleteMissSmsAdapter extends BaseAdapter{
	private Context mContext;
	private ArrayList<StateMissSmsModel> lstStateMissSms;
	private ContactHelper ctHelper;
	
	
	public DeleteMissSmsAdapter(Context mContext, ArrayList<StateMissSmsModel> listMissSmsData) {
		super();
		this.mContext = mContext;
		this.lstStateMissSms = listMissSmsData;
		ctHelper = new ContactHelper(mContext);
	}

	@Override
	public int getCount() {
		return lstStateMissSms.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int pos, View convertView, ViewGroup parent) {
		View v = convertView;
		if(v==null){
			LayoutInflater laInflater = LayoutInflater.from(mContext);
			v = laInflater.inflate(R.layout.ms_main_row, null);
			
		}
//		MissSms ms = lstStateMissSms.get(pos).ms;
		
		if (lstStateMissSms.get(pos).state) {
			// if True -> set background CYAN
			v.setBackgroundColor(Color.CYAN);
		} else {
			// if false -> set background TRANFERANT
			v.setBackgroundColor(Color.TRANSPARENT);
		}

		
		
		TextView phonenum = (TextView)v.findViewById(R.id.txtPhoneNoMS);
		phonenum.setText(lstStateMissSms.get(pos).ms.phoneno);
		TextView name = (TextView)v.findViewById(R.id.txtNameMS);
		name.setText(lstStateMissSms.get(pos).ms.name);
		TextView date = (TextView)v.findViewById(R.id.textDate);
		date.setText(lstStateMissSms.get(pos).ms.date);
		TextView time = (TextView)v.findViewById(R.id.txtTime);
		time.setText(lstStateMissSms.get(pos).ms.time);
		
		return v;
	}
}
