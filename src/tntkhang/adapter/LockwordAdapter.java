package tntkhang.adapter;

import java.util.ArrayList;

import tntkhang.database.LockWordHelper;
import tntkhang.model.LockwordModel;

import com.nonglamuniversity.disturbcall.R;



import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LockwordAdapter extends BaseAdapter{
	private Context mContext;
	private ArrayList<LockwordModel> lstLockWordData;
	private LockWordHelper lwHelper;
	
	public LockwordAdapter(Context mContext,ArrayList<LockwordModel> lstLockwordData){
		this.mContext = mContext;
		this.lstLockWordData = lstLockwordData;
		lwHelper = new LockWordHelper(mContext);
	}
	@Override
	public int getCount() {
		return lstLockWordData.size();
	}
	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public View getView(final int pos, View convertView, ViewGroup parent) {
		View v = convertView;
		if(v==null){
			LayoutInflater latInflater = LayoutInflater.from(mContext);
			v = latInflater.inflate(R.layout.ms_list_keyword_row, null);
			
			TextView content = (TextView) v.findViewById(R.id.sms_keyword);
			content.setText(lstLockWordData.get(pos).getContent());
			
			Button btnEdit = (Button)v.findViewById(R.id.sms_item_keyword_edit);
			btnEdit.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					View view = inflater.inflate(R.layout.ms_edit_keyword, null);
					final EditText editKeyword = (EditText)view.findViewById(R.id.sms_edittext_keyword);
					final LockwordModel	lockword = lstLockWordData.get(pos);
					editKeyword.setText(lockword.getContent());
					
					AlertDialog.Builder builder= new AlertDialog.Builder(mContext);
					builder.setTitle(mContext.getString(R.string.edt_key_word));
					builder.setView(view);
					builder.setCancelable(true);
					builder.setPositiveButton(mContext.getString(R.string.add), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							lockword.setContent(editKeyword.getText().toString());
							lstLockWordData.set(pos, lockword);
							lwHelper.updateLockword(lockword.getId(),lockword.getContent());
							notifyDataSetChanged();
						}
					});
					builder.setNegativeButton(mContext.getString(R.string.cancel), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
					AlertDialog dialog = builder.create();
					dialog.show();
				}
			});
			
			Button btnDelete = (Button)v.findViewById(R.id.sms_item_keyword_remove);
			btnDelete.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					final LockwordModel lockword = lstLockWordData.get(pos);
					AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
					builder.setTitle(mContext.getString(R.string.delete_key_word));
					builder.setMessage(mContext.getString(R.string.delete_key_word_question));
					builder.setCancelable(false);
					builder.setPositiveButton(mContext.getString(R.string.add), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
						Toast.makeText(mContext, "A: " + lockword.getContent().toString(), Toast.LENGTH_SHORT).show();                                                                                                                                                                                                                                                                                                                                                                                                                        	
							lstLockWordData.remove(pos);
							lwHelper.deleteLockword(lockword.getId());
							notifyDataSetChanged();
						}
					});
					builder.setNegativeButton(mContext.getString(R.string.cancel), new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
					AlertDialog dialog = builder.create();
					dialog.show();
				}
			});
		}
		return v;
	}
	
}
