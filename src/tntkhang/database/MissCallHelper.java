package tntkhang.database;

import java.util.ArrayList;

import tntkhang.model.ContactModel;
import tntkhang.model.MissCallModel;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MissCallHelper extends SQLiteOpenHelper {

	public static final String TABLE_NAME_MISSCALL = "misscall";
	public static final String ID_MISSCALL = "id";
	public static final String IDCONTACT_MISSCALL = "idcontact";
	public static final String DATE_MISSCALL = "date";
	public static final String TIME_MISSCALL = "time";
	private ContactHelper ctH;

	public MissCallHelper(Context context) {
		super(context, DatabaseHelper.DATABASE_NAME, null, 1);
		ctH = new ContactHelper(context);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

	public void saveMissCall(MissCallModel mc) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContactModel ct = ctH.getContactByPhonenumber(mc.phoneno);
		String sql = "insert into " + TABLE_NAME_MISSCALL + " values ('"
				+ mc.id + "', '" + ct.id + "', '" + mc.date + "', '"
				+ mc.time + "')";
		db.execSQL(sql);
		db.close();
	}

	public ArrayList<MissCallModel> getAllRecord() {
		SQLiteDatabase db = this.getWritableDatabase();
		// SELECT * FROM misscall, contact
		// where
		// contact.id = blacklist.id
		ArrayList<MissCallModel> list = new ArrayList<MissCallModel>();
		String sql = "select " + TABLE_NAME_MISSCALL + "." + ID_MISSCALL + ", " + IDCONTACT_MISSCALL + ", "
				+ DATE_MISSCALL + ", " + TIME_MISSCALL + " from "
				+ TABLE_NAME_MISSCALL + ", " + ContactHelper.TABLE_NAME_CONTACT
				+ " where " + MissCallHelper.TABLE_NAME_MISSCALL + "."
				+ MissCallHelper.IDCONTACT_MISSCALL + " = "
				+ ContactHelper.TABLE_NAME_CONTACT + "."
				+ ContactHelper.ID_CONTACT + " order by " + DATE_MISSCALL + ", " + TIME_MISSCALL + " desc";
		Cursor cursor = db.rawQuery(sql, null);
		try {
			if (cursor.moveToFirst()) {
				do {
					ContactModel ct = ctH.getContactById(cursor.getString(1));
					list.add(new MissCallModel(cursor.getString(0), ct.name, ct.phonenumber ,cursor.getString(2),  cursor.getString(3)));

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
		}

		cursor.close();
		db.close();

		return list;
	}

	public MissCallModel getMissCall(String id) {
		MissCallModel mc = null;
		SQLiteDatabase db = this.getReadableDatabase();
		String sql = "select * from " + TABLE_NAME_MISSCALL + " where "
				+ IDCONTACT_MISSCALL + "='" + id + "'";
		Cursor cursor = db.rawQuery(sql, null);
		while (cursor.moveToNext()) {
			String idmc = cursor.getString(0);
			String idcontact = cursor.getString(1);
			String date = cursor.getString(2);
			String time = cursor.getString(3);
			ContactModel ct = ctH.getContactById(idcontact);
			mc = new MissCallModel(idmc, ct.name, ct.phonenumber, date, time);
		}
		cursor.close();
		db.close();
		return mc;
	}

	public boolean isExitInMissCall(String id) {
		SQLiteDatabase db = this.getReadableDatabase();
		String sql = "select count(*) from " + TABLE_NAME_MISSCALL + " where "
				+ IDCONTACT_MISSCALL + "='" + id + "'";
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		int count = cursor.getInt(0);
		cursor.close();
		db.close();
		if (count > 0) {
			return true;
		}
		return false;
	}

	public void deleteContact(String id) {
		SQLiteDatabase db = this.getReadableDatabase();
		db.delete(TABLE_NAME_MISSCALL, ID_MISSCALL + " = '" + id + "'",
				null);
		db.close();
	}

	public ArrayList<MissCallModel> getAllRecordOfPhoneNo(String phonenumber) {
		SQLiteDatabase db = this.getWritableDatabase();
		// SELECT * FROM misscall, contact
		// where
		// contact.id = blacklist.id
		ArrayList<MissCallModel> list = new ArrayList<MissCallModel>();
		String sql = "select " + TABLE_NAME_MISSCALL + "." + ID_MISSCALL + ", " + IDCONTACT_MISSCALL + ", "
				+ DATE_MISSCALL + ", " + TIME_MISSCALL + " from "
				+ TABLE_NAME_MISSCALL + ", " + ContactHelper.TABLE_NAME_CONTACT
				+ " where " + MissCallHelper.TABLE_NAME_MISSCALL + "."
				+ MissCallHelper.IDCONTACT_MISSCALL + " = "
				+ ContactHelper.TABLE_NAME_CONTACT + "."
				+ ContactHelper.ID_CONTACT + " and " + ContactHelper.TABLE_NAME_CONTACT+"."+ContactHelper.PHONENUMBER_CONTACT + " = '" + phonenumber + "'" + " order by " + DATE_MISSCALL + ", " + TIME_MISSCALL + " desc";
		Log.i("S","SQL: " + sql);
		Cursor cursor = db.rawQuery(sql, null);
		try {
			if (cursor.moveToFirst()) {
				do {
					ContactModel ct = ctH.getContactById(cursor.getString(1));
					list.add(new MissCallModel(cursor.getString(0), ct.name, ct.phonenumber, cursor.getString(2), cursor
							.getString(3)));

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
		}

		cursor.close();
		db.close();

		return list;
	}
	public boolean stillExist(String id){
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "select count(*) from " + TABLE_NAME_MISSCALL
				+ " where " + IDCONTACT_MISSCALL + " = '" + id + "'";
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		int count = cursor.getInt(0);
		cursor.close();
		db.close();
		if (count > 0) {
			return true;
		}
		return false;
	}
}
