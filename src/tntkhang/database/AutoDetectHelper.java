package tntkhang.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AutoDetectHelper extends SQLiteOpenHelper {
	// create table fake call
	public static final String TABLE_NAME_AUTODETECT = "autodetect";
	public static final String IDCONTACT_AUTODETECT = "idcontact";
	public static final String TIMES_AUTODETECT = "times";
	private Context context;

	public AutoDetectHelper(Context context) {
		super(context, DatabaseHelper.DATABASE_NAME, null, 1);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

	// count all line in table fakecall
	public int countData() {
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "select count(*) from " + TABLE_NAME_AUTODETECT;
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToFirst();
		int count = cursor.getInt(0);
		cursor.close();
		db.close();
		return count;
	}

	public boolean isExitThisNumber(String idcontact) {
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "select count(*) from " + TABLE_NAME_AUTODETECT
				+ " where idcontact = '" + idcontact + "'";
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		int count = cursor.getInt(0);
		cursor.close();
		db.close();
		if (count > 0) {
			return true;
		}
		return false;
	}

	public int getNumberDisturb(String id) {
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "select " + TIMES_AUTODETECT + " from "
				+ TABLE_NAME_AUTODETECT + " where idcontact = '" + id + "'";
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		int count = cursor.getInt(0);
		cursor.close();
		db.close();
		return count;
	}

	public void deleteNumberOfFakeCall(String id) {
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "delete from " + TABLE_NAME_AUTODETECT
				+ " where "+ IDCONTACT_AUTODETECT +" = '" + id + "'";
		db.execSQL(sql);
		db.close();
	}

	public void insertCount(String idAddCount, int getCount) {
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "update " + TABLE_NAME_AUTODETECT + " set "
				+ TIMES_AUTODETECT + " = " + getCount + " where "
				+ IDCONTACT_AUTODETECT + " = '" + idAddCount + "'";
		Log.i("SQL","SSS: " + sql);
		db.execSQL(sql);
		db.close();
	}

	public void addNewNumber(String id) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(IDCONTACT_AUTODETECT, id);
		values.put(TIMES_AUTODETECT, 1);
		db.insert(TABLE_NAME_AUTODETECT, null, values);
		db.close();
	}

}
