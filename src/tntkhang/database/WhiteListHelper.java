package tntkhang.database;

//import model.Contact;
import java.util.ArrayList;

import tntkhang.model.ContactModel;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/*
 * Author: Khang Tran */
public class WhiteListHelper extends SQLiteOpenHelper {

	private Context context;

	public WhiteListHelper(Context context) {
		super(context, DatabaseHelper.DATABASE_NAME, null, 1);
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	public static final String TABLE_NAME_WHITELIST = "whitelist";
	public static final String IDCONTACT_WHITELIST = "idcontact";

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	public boolean isExitInWhiteList(String phonenumber) {
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "select count(*) from " + TABLE_NAME_WHITELIST + " , "
				+ ContactHelper.TABLE_NAME_CONTACT + " where "
				+ ContactHelper.TABLE_NAME_CONTACT + "."
				+ ContactHelper.ID_CONTACT + " = " + TABLE_NAME_WHITELIST + "."
				+ IDCONTACT_WHITELIST + " and "
				+ ContactHelper.TABLE_NAME_CONTACT + "."
				+ ContactHelper.PHONENUMBER_CONTACT + " = '" + phonenumber
				+ "'";
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		int count = cursor.getInt(0);
		cursor.close();
		db.close();
		if (count > 0) {
			return true;
		}
		return false;
	}

	public ArrayList<ContactModel> getAllListContactOfWhiteList() {
		SQLiteDatabase db = this.getWritableDatabase();
		// SELECT contact.id, phone, name, blocktype FROM contact, blacklist
		// where
		// contact.id = blacklist.id
		ArrayList<ContactModel> list = new ArrayList<ContactModel>();
		String sql = "select " + ContactHelper.TABLE_NAME_CONTACT + "."
				+ ContactHelper.ID_CONTACT + ","
				+ ContactHelper.PHONENUMBER_CONTACT + ","
				+ ContactHelper.NAME_CONTACT + "," + ContactHelper.BLOCK_TYPE
				+ " from " + ContactHelper.TABLE_NAME_CONTACT + ","
				+ WhiteListHelper.TABLE_NAME_WHITELIST + " where "
				+ ContactHelper.TABLE_NAME_CONTACT + "."
				+ ContactHelper.ID_CONTACT + "="
				+ WhiteListHelper.TABLE_NAME_WHITELIST + "."
				+ WhiteListHelper.IDCONTACT_WHITELIST;
		Cursor cursor = db.rawQuery(sql, null);

		try {
			if (cursor.moveToFirst()) {
				do {
					list.add(new ContactModel(cursor.getString(0), cursor
							.getString(1), cursor.getString(2), cursor
							.getInt(3)));

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
		}
		db.close();
		return list;
	}

	public void addNumber(String iDPM) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(IDCONTACT_WHITELIST, iDPM);

		db.insert(TABLE_NAME_WHITELIST, null, values);
		db.close();

	}

	public void deleteContact(String id) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_NAME_WHITELIST,
				IDCONTACT_WHITELIST + " = '" + id + "'", null);
		db.close();
	}

	public void updateContact(String id, ContactModel ct) {
		SQLiteDatabase db = this.getWritableDatabase();
		String whereClause = "id='" + ct.id + "'";
		ContentValues cv = new ContentValues();
		cv.put(ContactHelper.ID_CONTACT, ct.id);
		cv.put(ContactHelper.PHONENUMBER_CONTACT, ct.phonenumber);
		cv.put(ContactHelper.NAME_CONTACT, ct.name);
		cv.put(ContactHelper.BLOCK_TYPE, ct.blocktype);
		db.update(ContactHelper.TABLE_NAME_CONTACT, cv, whereClause, null);
		db.close();
	}

}
