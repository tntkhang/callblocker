package tntkhang.database;

import java.util.ArrayList;

import tntkhang.model.ContactModel;
import tntkhang.model.MissSmsModel;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MissSmsHelper extends SQLiteOpenHelper {
	private final Context mContext;
	private SQLiteDatabase mDB;
	private DatabaseHelper mDBHelper;
	
	public MissSmsHelper(Context context) {
		super(context, DatabaseHelper.DATABASE_NAME, null, 1);
		this.mContext = context;
	}

	public static final String TABLE_NAME_MISSSMS = "misssms";
	public static final String ID_MISSSMS = "id";
	public static final String IDCONTACT_MISSSMS = "idcontact";
	public static final String CONTENT_MISSSMS = "content";
	public static final String DATE_MISSSMS = "date";
	public static final String TIME_MISSSMS = "time";

	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}
	public void saveMissSMS(MissSmsModel ms){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		ContactHelper ct = new ContactHelper(mContext);
		ContactModel c = ct.getContactByPhonenumber(ms.phoneno);
		values.put(MissSmsHelper.ID_MISSSMS, ms.id);
		values.put(MissSmsHelper.IDCONTACT_MISSSMS, c.id);
		values.put(MissSmsHelper.CONTENT_MISSSMS, ms.content);
		values.put(MissSmsHelper.DATE_MISSSMS, ms.date);
		values.put(MissSmsHelper.TIME_MISSSMS, ms.time);
		db.insert(MissSmsHelper.TABLE_NAME_MISSSMS, null, values);
		db.close();
	}
	public Cursor getAllListMissSms(){
		openDB();
		String sql = "select " +ContactHelper.TABLE_NAME_CONTACT+"."
				+ContactHelper.ID_CONTACT+","
				+ContactHelper.PHONENUMBER_CONTACT+","
				+ContactHelper.NAME_CONTACT+", "
				+MissSmsHelper.TABLE_NAME_MISSSMS+"."
				+MissSmsHelper.ID_MISSSMS+","
				+MissSmsHelper.CONTENT_MISSSMS+","
				+MissSmsHelper.DATE_MISSSMS+","
				+MissSmsHelper.TIME_MISSSMS+" from "
				+ContactHelper.TABLE_NAME_CONTACT+","
				+MissSmsHelper.TABLE_NAME_MISSSMS+ " where "
				+ ContactHelper.TABLE_NAME_CONTACT + "."
				+ ContactHelper.ID_CONTACT + "="
				+ MissSmsHelper.TABLE_NAME_MISSSMS+ "."
				+ MissSmsHelper.IDCONTACT_MISSSMS;
		Cursor cursor = mDB.rawQuery(sql, null);
		return cursor;
	}
	
	public ArrayList<MissSmsModel> getAll(){
		openDB();
		ArrayList<MissSmsModel> list = new ArrayList<MissSmsModel>();
		ContactHelper ctH = new ContactHelper(mContext);
		String sql = "select " + TABLE_NAME_MISSSMS + "." + ID_MISSSMS + ", " + IDCONTACT_MISSSMS + ", " + CONTENT_MISSSMS +", "
				+ DATE_MISSSMS + ", " + TIME_MISSSMS + " from "
				+ TABLE_NAME_MISSSMS + ", " + ContactHelper.TABLE_NAME_CONTACT
				+ " where " + MissSmsHelper.TABLE_NAME_MISSSMS + "."
				+ MissSmsHelper.IDCONTACT_MISSSMS + " = "
				+ ContactHelper.TABLE_NAME_CONTACT + "."
				+ ContactHelper.ID_CONTACT + " order by " + DATE_MISSSMS + ", " + TIME_MISSSMS + " desc";
		Cursor cursor = mDB.rawQuery(sql, null);
		try {
			if (cursor.moveToFirst()) {
				do {
					ContactModel ct = ctH.getContactById(cursor.getString(1));
					list.add(new MissSmsModel(cursor.getString(0), ct.name, ct.phonenumber, cursor.getString(2), cursor
							.getString(3),cursor
							.getString(4)));

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
		}

		cursor.close();
		closeDB();

		return list;
	}
	
	
	public void remove(String id){
		openDB();
		mDB.delete(TABLE_NAME_MISSSMS, ID_MISSSMS + " = '" + id + "'",
				null);
		closeDB();
	}
	public MissSmsHelper openDB(){
		mDBHelper = new DatabaseHelper(mContext);
		mDB = mDBHelper.getWritableDatabase();
		return this;
	}
	public void closeDB(){
		mDBHelper.close();
	}

	public void deleteAll() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_NAME_MISSSMS, null, null);
		db.close();
	}

	public ArrayList<MissSmsModel> getAllRecordOfPhoneNo(String phoneNo) {
		SQLiteDatabase db = this.getWritableDatabase();
		ArrayList<MissSmsModel> list = new ArrayList<MissSmsModel>();
		ContactHelper ctH = new ContactHelper(mContext);
		String sql = "select " + TABLE_NAME_MISSSMS + "." + ID_MISSSMS + ", " + IDCONTACT_MISSSMS + ", " + CONTENT_MISSSMS + ", "
				+ DATE_MISSSMS + ", " + TIME_MISSSMS + " from "
				+ TABLE_NAME_MISSSMS + ", " + ContactHelper.TABLE_NAME_CONTACT
				+ " where " + MissSmsHelper.TABLE_NAME_MISSSMS + "."
				+ MissSmsHelper.IDCONTACT_MISSSMS + " = "
				+ ContactHelper.TABLE_NAME_CONTACT + "."
				+ ContactHelper.ID_CONTACT + " and " + ContactHelper.TABLE_NAME_CONTACT+"."+ContactHelper.PHONENUMBER_CONTACT + " = '" + phoneNo + "'" + " order by " + DATE_MISSSMS + ", " + TIME_MISSSMS + " desc";
		Log.i("S","SQL: " + sql);
		Cursor cursor = db.rawQuery(sql, null);
		try {
			if (cursor.moveToFirst()) {
				do {
					ContactModel ct = ctH.getContactById(cursor.getString(1));
					list.add(new MissSmsModel(cursor.getString(0), ct.name, ct.phonenumber, cursor.getString(2), cursor
							.getString(3), cursor.getString(4)));

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
		}

		cursor.close();
		db.close();

		return list;
	}
	public boolean stillExist(String id){
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "select count(*) from " + TABLE_NAME_MISSSMS
				+ " where " + IDCONTACT_MISSSMS + " = '" + id + "'";
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		int count = cursor.getInt(0);
		cursor.close();
		db.close();
		if (count > 0) {
			return true;
		}
		return false;
	}
}
