package tntkhang.database;

//import model.Contact;
//import model.StateSelectContact;
import java.util.ArrayList;

import tntkhang.model.ContactModel;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/*
 * Author: Khang Tran*/
public class BlackListHelper extends SQLiteOpenHelper {
	private final Context mContext;
	private SQLiteDatabase mDB;
	private DatabaseHelper mDBHelper;

	// information blacklist
	public static final String TABLE_NAME_BLACKLIST = "blacklist";
	public static final String IDCONTACT_BLACKLIST = "idcontact";
	public static final String TYPEBLOCK_BLACKLIST = "typeblock";

	public BlackListHelper(Context context) {
		super(context, DatabaseHelper.DATABASE_NAME, null, 1);
		this.mContext = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	public BlackListHelper openDB() {
		mDBHelper = new DatabaseHelper(mContext);
		mDB = mDBHelper.getWritableDatabase();
		return this;
	}

	public void closeDB() {
		mDBHelper.close();
	}

	public boolean isExitInBlackList(String phonenumber) {
		SQLiteDatabase db = this.getWritableDatabase();
		// select * from blacklist, contact where contact.id = blacklist.id and
		// contact.phonenumber = phonenumber
		String sql = "select count(*) from " + TABLE_NAME_BLACKLIST + " , "
				+ ContactHelper.TABLE_NAME_CONTACT + " where "
				+ ContactHelper.TABLE_NAME_CONTACT + "."
				+ ContactHelper.ID_CONTACT + " = " + TABLE_NAME_BLACKLIST + "."
				+ IDCONTACT_BLACKLIST + " and "
				+ ContactHelper.TABLE_NAME_CONTACT + "."
				+ ContactHelper.PHONENUMBER_CONTACT + " = '" + phonenumber
				+ "'";
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		int count = cursor.getInt(0);
		cursor.close();
		db.close();
		if (count > 0) {
			return true;
		}
		return false;
	}

	public void addBlackNumber(String idContact) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(IDCONTACT_BLACKLIST, idContact);

		db.insert(TABLE_NAME_BLACKLIST, null, values);
		db.close();
	}

	public void deleteAll() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_NAME_BLACKLIST, null, null);
		db.close();
	}

	public void deleteContact(String phone) {
		openDB();
		String sql = "DELETE FROM " + BlackListHelper.TABLE_NAME_BLACKLIST
				+ " WHERE " + BlackListHelper.IDCONTACT_BLACKLIST + " = '"
				+ phone + "'";

		mDB.execSQL(sql);
		closeDB();
	}

	public void updateContact(String id, ContactModel ct) {
		openDB();
		String whereClause = "id='" + ct.id + "'";
		ContentValues cv = new ContentValues();
		cv.put(ContactHelper.ID_CONTACT, ct.id);
		cv.put(ContactHelper.PHONENUMBER_CONTACT, ct.phonenumber);
		cv.put(ContactHelper.NAME_CONTACT, ct.name);
		cv.put(ContactHelper.BLOCK_TYPE, ct.blocktype);
		mDB.update(ContactHelper.TABLE_NAME_CONTACT, cv, whereClause, null);
		closeDB();
	}

	public ArrayList<ContactModel> getAllListContactOfBlackList() {
		openDB();
		// SQLiteDatabase db = this.getWritableDatabase();
		// SELECT contact.id, phone, name, blocktype FROM contact, blacklist
		// where
		// contact.id = blacklist.id
		ArrayList<ContactModel> list = new ArrayList<ContactModel>();
		String sql = "select " + ContactHelper.TABLE_NAME_CONTACT + "."
				+ ContactHelper.ID_CONTACT + ","
				+ ContactHelper.PHONENUMBER_CONTACT + ","
				+ ContactHelper.NAME_CONTACT + "," + ContactHelper.BLOCK_TYPE
				+ " from " + ContactHelper.TABLE_NAME_CONTACT + ","
				+ BlackListHelper.TABLE_NAME_BLACKLIST + " where "
				+ ContactHelper.TABLE_NAME_CONTACT + "."
				+ ContactHelper.ID_CONTACT + "="
				+ BlackListHelper.TABLE_NAME_BLACKLIST + "."
				+ BlackListHelper.IDCONTACT_BLACKLIST;
		Cursor cursor = mDB.rawQuery(sql, null);
		try {
			if (cursor.moveToFirst()) {
				do {
					list.add(new ContactModel(cursor.getString(0), cursor
							.getString(1), cursor.getString(2), cursor
							.getInt(3)));

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
		}
		closeDB();
		return list;
	}
	public boolean stillExist(String id){
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "select count(*) from " + TABLE_NAME_BLACKLIST
				+ " where " + IDCONTACT_BLACKLIST + " = '" + id + "'";
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		int count = cursor.getInt(0);
		cursor.close();
		db.close();
		if (count > 0) {
			return true;
		}
		return false;
	}
}
