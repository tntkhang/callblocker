package tntkhang.database;

import java.sql.Date;
import java.util.ArrayList;

import tntkhang.model.ContactModel;

//import model.CallLogModel;
//import model.StateSelectContact;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class ContactHelper extends SQLiteOpenHelper {
	/*
	 * Author: Khang Tran
	 */
	// information contact
	public static final String TABLE_NAME_CONTACT = "contact";
	public static final String ID_CONTACT = "id";
	public static final String PHONENUMBER_CONTACT = "phonenumber";
	public static final String NAME_CONTACT = "name";
	public static final String BLOCK_TYPE = "blocktype";

	public Context context;

	public ContactHelper(Context context) {
		super(context, DatabaseHelper.DATABASE_NAME, null, 1);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

	// add contact
	public boolean addContact(ContactModel contact) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ContactHelper.ID_CONTACT, contact.id);
		values.put(ContactHelper.PHONENUMBER_CONTACT, contact.phonenumber);
		values.put(ContactHelper.NAME_CONTACT, contact.name);
		values.put(ContactHelper.BLOCK_TYPE, contact.blocktype);
		db.insert(ContactHelper.TABLE_NAME_CONTACT,
				ContactHelper.PHONENUMBER_CONTACT, values);
		db.close();

		return true;
	}

	public void deleteContact(String phone) {
		SQLiteDatabase db = this.getWritableDatabase();
		String sql2 = "DELETE FROM " + ContactHelper.TABLE_NAME_CONTACT
				+ " WHERE " + ContactHelper.TABLE_NAME_CONTACT + "."
				+ ContactHelper.PHONENUMBER_CONTACT + " = '" + phone + "'";
		db.execSQL(sql2);
		db.close();
	}

	public boolean isExitThisNumber(String number) {
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "select count(*) from " + TABLE_NAME_CONTACT
				+ " where phonenumber = '" + number + "'";
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToNext();
		int count = cursor.getInt(0);
		cursor.close();
		db.close();
		if (count > 0) {
			return true;
		}
		return false;
	}

	
	 public void deleteContactFromNumberPhone(String phone) {
	 SQLiteDatabase db = this.getWritableDatabase();
	 String sql2 = "DELETE FROM " + ContactHelper.TABLE_NAME_CONTACT
	 + " WHERE " + ContactHelper.TABLE_NAME_CONTACT + "."
	 + ContactHelper.PHONENUMBER_CONTACT + " = '" + phone + "'";
	 db.execSQL(sql2);
	 db.close();
	 }
	
	// add contact
	public boolean addFromContactToBL(ArrayList<ContactModel> listContacts) {
		SQLiteDatabase db = this.getWritableDatabase();
		BlackListHelper blackListHelper = new BlackListHelper(context);

		for (int i = 0; i < listContacts.size(); i++) {
			ContactModel contact = listContacts.get(i);
			String IDPM = Long.toHexString(
					Double.doubleToLongBits(Math.random())).toUpperCase();
			ContentValues values = new ContentValues();
			values.put(ContactHelper.ID_CONTACT, IDPM);
			values.put(ContactHelper.PHONENUMBER_CONTACT, contact.phonenumber);
			values.put(ContactHelper.NAME_CONTACT, contact.name);
			values.put(ContactHelper.BLOCK_TYPE, 3);

			db.insert(ContactHelper.TABLE_NAME_CONTACT,
					ContactHelper.PHONENUMBER_CONTACT, values);
			blackListHelper.addBlackNumber(IDPM);
		}
		db.close();
		return true;
	}
	public boolean addFromContactToWL(ArrayList<ContactModel> listContacts) {
		SQLiteDatabase db = this.getWritableDatabase();
		WhiteListHelper wlHelper = new WhiteListHelper(context);

		for (int i = 0; i < listContacts.size(); i++) {
			ContactModel contact = listContacts.get(i);
			String IDPM = Long.toHexString(
					Double.doubleToLongBits(Math.random())).toUpperCase();
			ContentValues values = new ContentValues();
			values.put(ContactHelper.ID_CONTACT, IDPM);
			values.put(ContactHelper.PHONENUMBER_CONTACT, contact.phonenumber);
			values.put(ContactHelper.NAME_CONTACT, contact.name);
			values.put(ContactHelper.BLOCK_TYPE, 3);

			db.insert(ContactHelper.TABLE_NAME_CONTACT,
					ContactHelper.PHONENUMBER_CONTACT, values);
			wlHelper.addNumber(IDPM);
		}
		db.close();
		return true;
	}

	// get contact by idContact
	public ContactModel getContactById(String id) {
		ContactModel contact = null;
		SQLiteDatabase db = this.getReadableDatabase();
		String sql = "select * from " + TABLE_NAME_CONTACT + " where "
				+ TABLE_NAME_CONTACT + "." + ID_CONTACT + "='" + id + "'";
		Cursor cursor = db.rawQuery(sql, null);
		while (cursor.moveToNext()) {
			String idContact = cursor.getString(0);
			String phonenumber = cursor.getString(1);
			String name = cursor.getString(2);
			int blocktype = cursor.getInt(3);
			contact = new ContactModel(idContact, phonenumber, name, blocktype);
		}
		cursor.close();
		db.close();
		return contact;
	}

	//
	// get contact by idContact
	public ContactModel getContactByPhonenumber(String phoneNumber) {
		ContactModel contact = null;
		SQLiteDatabase db = this.getReadableDatabase();
		String sql = "select * from " + TABLE_NAME_CONTACT + " where "
				+ TABLE_NAME_CONTACT + "." + PHONENUMBER_CONTACT + "='"
				+ phoneNumber + "'";
		Cursor cursor = db.rawQuery(sql, null);
		while (cursor.moveToNext()) {
			String idContact = cursor.getString(0);
			String phonenumber = cursor.getString(1);
			String name = cursor.getString(2);
			int blocktype = cursor.getInt(3);
			contact = new ContactModel(idContact, phonenumber, name, blocktype);
		}
		cursor.close();
		db.close();
		return contact;
	}

	public String getIdContactByPhonenumber(String phoneNumber) {
		SQLiteDatabase db = this.getReadableDatabase();
		String sql = "select " + ID_CONTACT + " from " + TABLE_NAME_CONTACT
				+ " where " + TABLE_NAME_CONTACT + "." + PHONENUMBER_CONTACT
				+ "='" + phoneNumber + "'";
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToFirst();
		String idContact = cursor.getString(0);
		cursor.close();
		db.close();
		return idContact;
	}

	public void deleteContactID(String id) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_NAME_CONTACT, ID_CONTACT + " = '" + id +"'", null);
		db.close();
	}

	

}
