package tntkhang.database;

import java.util.ArrayList;

import tntkhang.model.LockwordModel;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/*
 * Author: Khang Tran */
public class LockWordHelper extends SQLiteOpenHelper {

	public static final String TABLE_NAME_LOCKWORD = "lockword";
	public static final String ID_LOCKWORD = "id";
	public static final String CONTENT_LOCKWORD = "content";
	private DatabaseHelper mDBHelper;
	private final Context mContext;
	private SQLiteDatabase mDB;
	
	public LockWordHelper(Context context) {
		super(context, DatabaseHelper.DATABASE_NAME, null, 1);
		this.mContext = context;
	}	

	@Override
	public void onCreate(SQLiteDatabase arg0) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

	}
	public void addKeyWord(String idPM, String keyword) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(LockWordHelper.ID_LOCKWORD, idPM);
		values.put(LockWordHelper.CONTENT_LOCKWORD, keyword);
		db.insert(LockWordHelper.TABLE_NAME_LOCKWORD,LockWordHelper.CONTENT_LOCKWORD, values);
		db.close();
	}

	public ArrayList<LockwordModel> loadLockWord(){
		openDB();
		String sql = "SELECT "+LockWordHelper.TABLE_NAME_LOCKWORD+"."+LockWordHelper.ID_LOCKWORD+","+LockWordHelper.CONTENT_LOCKWORD+" FROM "+LockWordHelper.TABLE_NAME_LOCKWORD;
		Cursor cursor = mDB.rawQuery(sql, null);
		ArrayList<LockwordModel> lstLockwordData = new ArrayList<LockwordModel>();
		try{
			if(cursor.moveToFirst()){
				do{
					lstLockwordData.add(new LockwordModel(cursor.getString(0), cursor.getString(1)));
				}while(cursor.moveToNext());
			}
		}catch (Exception e) {
		}closeDB();
		return lstLockwordData;
	}
	public void closeDB(){
		mDBHelper.close();
	}
	
	public LockWordHelper openDB(){
		mDBHelper = new DatabaseHelper(mContext);
		mDB = mDBHelper.getWritableDatabase();
		return this;
	}

	public void updateLockword(String id, String content) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(LockWordHelper.CONTENT_LOCKWORD, content);
		db.update(LockWordHelper.TABLE_NAME_LOCKWORD, values, LockWordHelper.ID_LOCKWORD+"=?", new String[]{id});
		db.close();
	}

	public void deleteLockword(String id) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(LockWordHelper.TABLE_NAME_LOCKWORD, LockWordHelper.ID_LOCKWORD+"=?", new String[]{id});
		db.close();
	}
}
