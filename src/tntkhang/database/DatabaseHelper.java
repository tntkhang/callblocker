package tntkhang.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
	/*
	 * Author: Khang Tran
	 */
	// create database when application run
	public static final String DATABASE_NAME = "androidcallsblocker";
	private static String KEY = "tntkhang";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, 1);
		System.out.println("Database Android Calls Blocker");
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		System.out.println("Create database......");

		// table contact
		db.execSQL("create table " + ContactHelper.TABLE_NAME_CONTACT + "("
				+ ContactHelper.ID_CONTACT + " nvarchar(50) primary key, "
				+ ContactHelper.PHONENUMBER_CONTACT + " nvarchar(50), "
				+ ContactHelper.NAME_CONTACT + " nvarchar(100), "
				+ ContactHelper.BLOCK_TYPE + " int)");

		// table auto detect
		db.execSQL("create table " + AutoDetectHelper.TABLE_NAME_AUTODETECT
				+ "(" + AutoDetectHelper.IDCONTACT_AUTODETECT
				+ " nvarchar(50) primary key, "
				+ AutoDetectHelper.TIMES_AUTODETECT
				+ " nvarchar(100),FOREIGN KEY("
				+ AutoDetectHelper.IDCONTACT_AUTODETECT + ") REFERENCES "
				+ ContactHelper.TABLE_NAME_CONTACT + "("
				+ ContactHelper.ID_CONTACT + "))");

		// table black list
		db.execSQL("create table " + BlackListHelper.TABLE_NAME_BLACKLIST + "("
				+ BlackListHelper.IDCONTACT_BLACKLIST
				+ " nvarchar(50) primary key,FOREIGN KEY("
				+ BlackListHelper.IDCONTACT_BLACKLIST + ") REFERENCES "
				+ ContactHelper.TABLE_NAME_CONTACT + "("
				+ ContactHelper.ID_CONTACT + "))");

		// table lock word
		db.execSQL("create table " + LockWordHelper.TABLE_NAME_LOCKWORD + "("
				+ LockWordHelper.ID_LOCKWORD + " nvarchar(50) primary key,"
				+ LockWordHelper.CONTENT_LOCKWORD + " text)");

		// table miss call
		db.execSQL("create table " + MissCallHelper.TABLE_NAME_MISSCALL + "("
				+ MissCallHelper.ID_MISSCALL + " nvarchar(50) primary key,"
				+ MissCallHelper.IDCONTACT_MISSCALL + " nvarchar(50),"
				+ MissCallHelper.DATE_MISSCALL + " date,"
				+ MissCallHelper.TIME_MISSCALL + " time)");

		// table miss sms
		db.execSQL("create table " + MissSmsHelper.TABLE_NAME_MISSSMS + "("
				+ MissSmsHelper.ID_MISSSMS + " nvarchar(50)  primary key,"
				+ MissSmsHelper.IDCONTACT_MISSSMS + " nvarchar(50),"
				+ MissSmsHelper.CONTENT_MISSSMS + " text,"
				+ MissSmsHelper.DATE_MISSSMS + " date,"
				+ MissSmsHelper.TIME_MISSSMS + " time)");

		// table white list
		db.execSQL("create table " + WhiteListHelper.TABLE_NAME_WHITELIST + "("
				+ WhiteListHelper.IDCONTACT_WHITELIST
				+ " nvarchar(50) primary key, FOREIGN KEY("
				+ WhiteListHelper.IDCONTACT_WHITELIST + ") REFERENCES "
				+ ContactHelper.TABLE_NAME_CONTACT + "("
				+ ContactHelper.ID_CONTACT + "))");

		// table setting
		/*
		 * enable 0 - null, 1 - black list, 2- white list
		 */
		db.execSQL("create table " + SettingHelper.TABLE_NAME_SETTING + "("
				+ SettingHelper.ID + " nvarchar(50) primary key,"
				+ SettingHelper.ENABLE + " tinyint, " + SettingHelper.SHOWICON
				+ " tinyint, " + SettingHelper.VIETTEL + " tinyint, "
				+ SettingHelper.VINA + " tinyint, " + SettingHelper.MOBI
				+ " tinyint, " + SettingHelper.GMOBILE + " tinyint, "
				+ SettingHelper.VNMOBILE + " tinyint, "
				+ SettingHelper.START_WITH + " text, " + SettingHelper.END_WITH
				+ " text, " + SettingHelper.VALUE_DETECT + " int)");
		// create default value for Setting Table
		db.execSQL("insert into " + SettingHelper.TABLE_NAME_SETTING
				+ " values ('" + KEY + "', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0)");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		System.out.println("update table");
		db.execSQL("drop table if exists " + MissSmsHelper.TABLE_NAME_MISSSMS);
		db.execSQL("drop table if exists " + MissCallHelper.TABLE_NAME_MISSCALL);
		db.execSQL("drop table if exists "
				+ AutoDetectHelper.TABLE_NAME_AUTODETECT);
		db.execSQL("drop table if exists "
				+ WhiteListHelper.TABLE_NAME_WHITELIST);
		db.execSQL("drop table if exists "
				+ BlackListHelper.TABLE_NAME_BLACKLIST);
		db.execSQL("drop table if exists " + LockWordHelper.TABLE_NAME_LOCKWORD);
		db.execSQL("drop table if exists " + ContactHelper.TABLE_NAME_CONTACT);
		db.execSQL("drop table if exists " + SettingHelper.TABLE_NAME_SETTING);
		onCreate(db);
	}

	// count table has in database
	public int countTable() {
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "SELECT count(*) FROM sqlite_master WHERE type = 'table' AND name != 'android_metadata' AND name != 'sqlite_sequence'";
		Cursor cursor = db.rawQuery(sql, null);
		cursor.moveToFirst();
		int count = cursor.getInt(0);
		cursor.close();
		db.close();
		return count;
	}

}
