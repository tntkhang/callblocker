package tntkhang.database;

import tntkhang.model.SettingConfigModel;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SettingHelper extends SQLiteOpenHelper {

	public static String TABLE_NAME_SETTING = "setting";
	public static String ID = "id";
	public static String ENABLE = "enable";
	public static String SHOWICON = "showicon";
	public static String VIETTEL = "viettel";
	public static String VINA = "vina";
	public static String MOBI = "mobi";
	public static String VNMOBILE = "vnmobile";
	public static String GMOBILE = "gmobile";
	public static String START_WITH = "start";
	public static String END_WITH = "end";
	public static String VALUE_DETECT = "value";
	private static String KEY = "tntkhang";

	public SettingHelper(Context context) {
		super(context, DatabaseHelper.DATABASE_NAME, null, 1);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}
	public SettingConfigModel loadSetting(){
		SQLiteDatabase db = this.getWritableDatabase();
		String sql = "SELECT * FROM " + TABLE_NAME_SETTING;
		
		Cursor cr = db.rawQuery(sql, null);
		cr.moveToFirst();
		
		int ena = cr.getInt(1);
		int showicon = cr.getInt(2);
		int viettel = cr.getInt(3);
		int vina = cr.getInt(4);
		int mobi = cr.getInt(5);
		int vnmobile = cr.getInt(6);
		int gmobile = cr.getInt(7);
		int startW = cr.getInt(8);
		int endW = cr.getInt(9);
		int valueDetect = cr.getInt(10);
		
		db.close();
		SettingConfigModel st = new SettingConfigModel(ena,showicon, viettel, vina, mobi, vnmobile, gmobile, startW, endW, valueDetect);
		return st;
		
	}
	public void saveSetting(SettingConfigModel st){
		SQLiteDatabase db = this.getWritableDatabase();
		String whereClause = "id = '" + KEY +"'";
		ContentValues ct = new ContentValues();
		ct.put(SettingHelper.ENABLE, st.enable);
		ct.put(SettingHelper.SHOWICON, st.showicon);
		ct.put(SettingHelper.VIETTEL, st.viettel);
		ct.put(SettingHelper.VINA, st.vina);
		ct.put(SettingHelper.MOBI, st.mobi);
		ct.put(SettingHelper.VNMOBILE, st.vnmobile);
		ct.put(SettingHelper.GMOBILE, st.gmobile);
		ct.put(SettingHelper.START_WITH, st.startWith);
		ct.put(SettingHelper.END_WITH, st.endWith);
		ct.put(SettingHelper.VALUE_DETECT, st.valueDetect);
		
		db.update(SettingHelper.TABLE_NAME_SETTING, ct, whereClause, null);
		db.close();
	}
	
}
